<?php

return [
    'warning.title' => 'Attention!',
    'warning.msg' => 'The project is under development, errors may occur! Soon the main page will be updated.',
];
