<?php

return [
    'profile' => 'Мой профиль',
    'balance' => 'Мой баланс',
    'settings' => 'Настройки',
    'logout' => 'Выйти',
];
