<?php

return [
    'warning.title' => 'Внимание!',
    'warning.msg' => 'Проект находится в стадии разработки, могут возникать ошибки!',
    'button.auth' => 'Войти',
    'button.auth.steam' => 'Зарегистрироваться через Steam',
    'button.remember' => 'Запомнить меня',
    'button.forgot.pwd' => 'Забыли пароль?'
];
