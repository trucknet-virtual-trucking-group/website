<div id="header">
    <div id="header_inner">
        <div id="header_inner_overflow">
            <div id="header_inner_left">
                <a href="@if (Auth::check()){{ route('news') }}@else{{ route('index') }}@endif" onclick="nav.go(this); return false">
                    <span id="logo"></span>
                </a>
            </div>
            <div id="header_inner_right">
                @if (!Auth::check())
                    <div id="header_auth_form">
                        <div id="header_auth_form_fields">
                            <div id="header_auth_form_fields_overflow">
                                <div id="header_auth_form_error_msg"></div>
                                <div id="header_auth_form_login">
                                    <div class="title">Логин:</div>
                                    <input type="text" id="auth_login" iplaceholder="Введите логин" class="placeholder">
                                </div>
                                <div id="header_auth_form_password">
                                    <div id="header_auth_form_fields_restore_link" title="Забыли пароль?">
                                        <div>
                                            <a href="{{ route('restore') }}" onclick="nav.go(this); return false">
                                                <span id="header_auth_form_fields_restore_icon"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="title">Пароль:</div>
                                    <input type="password" id="auth_password" iplaceholder="Введите пароль"
                                           class="placeholder">
                                </div>
                                <div id="header_auth_form_buttons">
                                    <div class="title">&nbsp;</div>
                                    <div onclick="users.auth();" id="auth_submit_button" class="button user_select_none">
                                        <div class="button_inner">Войти</div>
                                    </div>
                                    <div onclick="users.wnd_steam();" id="auth_reg_button" class="button user_select_none">
                                        <div class="button_inner">Регистрация</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="profile fl_r">
                        <div class="clearfix">
                            <div class="avatar_wrap fl_l">
                                <a href="{{ route('user.profile', Auth::user()->id) }}" onclick="nav.go(this); return false">
                                  <img src="{{ Auth::user()->getSmallAvatar() }}" alt="avatar" class="br3px user_select_none">
                                    @php
                                        Auth::user()->getSmallAvatar();
                                        Auth::user()->getSmallAvatar();
                                        Auth::user()->getSmallAvatar();
                                        Auth::user()->getSmallAvatar();
                                        Auth::user()->getSmallAvatar();
                                        Auth::user()->getSmallAvatar();
                                        Auth::user()->getSmallAvatar();
                                        Auth::user()->getSmallAvatar();
                                        Auth::user()->getSmallAvatar();
                                    @endphp
                                </a>
                                <div class="upload_preloader">
                                    <span class="upload upload_white"><span></span><span></span><span></span></span>
                                </div>
                            </div>
                            <div class="info fl_l">
                                <a href="{{ route('user.profile', Auth::user()->id) }}" onclick="nav.go(this); return false">
                                    <div class="fullname">
                                        {{ Auth::user()->fullName() }}
                                    </div>
                                    <a/>
                                <div class="logout">
                                    <a href="{{ route('logout') }}" class="trns15s">Выйти</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="header_balance fl_r">
                        <a href="{{ route('balance') }}" class="trns15s br15px" onclick="nav.go(this); return false">0 баллов</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@if (!Auth::check())
    @push('main_scripts')
        @modifiedScript
        <script>
            $('#auth_login').attr('iplaceholder', 'Введите логин');
            $('#auth_password').attr('iplaceholder', 'Введите пароль');

            iplaceholder('#auth_login');
            iplaceholder('#auth_password');
        </script>
        @endmodifiedScript
    @endpush
@endif
