@php
    /** @var \App\Models\User\Wall\Wall $wall */
    $show_like_count = $wall->likes_count ? $wall->likes_count : '';
    $is_active_current_user = $wall->currentLike ? 'active' : '';
    $is_empty = $wall->likes_count ? '' : 'empty';
@endphp

<article class="feed_list_item br3px" id="feed_{{ $wall->id }}">
    <div class="feed_list_item_header">
        <div class="avatar">
            <a href="{{ route('user.profile', $wall->user->id) }}" onclick="nav.go(this); return false">
                <img src="{{ $wall->user->getSmallAvatar() }}" alt="avatar">
            </a>
        </div>
        <div class="header_content">
            <div class="user_name">
                <a href="{{ route('user.profile', $wall->user->id) }}"
                   onclick="nav.go(this); return false">{{ $wall->user->fullName() }}</a>
            </div>
            <div class="time_add">
                <a class="time" tabindex="0" role="link" title="{{ $wall->created_at }}">{{ $wall->created_at->diffForHumans() }}</a>
            </div>
            <div class="actions_menu_wrap" onmouseover="Wall.actionsMenuShow(this);"
                 onmouseout="Wall.actionsMenuHide(this);">
                <div class="actions_menu_icons">
                    <div class="blind_label">
                        Действия
                    </div>
                </div>
                <div class="actions_menu">
                    @if (Auth::user()->id === $wall->user->id)
                        <a class="actions_menu_item user_select_none" tabindex="0" role="link"
                           onclick="Wall.actionsMenuDelete({{ $wall->id }});">
                            Удалить
                        </a>
                    @else
                        <a class="actions_menu_item user_select_none" tabindex="0" role="link">Пожаловаться</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="feed_content">
        <div class="text_wrap">
            {!! $wall->text !!}
        </div>
        <div class="like_wrap">
            <div class="like_c">
                <div class="like_btns">
                    <a class="like_btn like {{ $is_active_current_user }} {{ $is_empty }}" tabindex="0" role="link" title="Нравится" onclick="Likes.toggle(this, event, {{ $wall->id }});">
                        <div class="like_button_icon"></div>
                        <div class="like_button_label"></div>
                        <div class="like_button_count">{{ $show_like_count }}</div>
                        <span class="blind_label">Нравится</span>
                    </a>
                    <a class="like_btn comment" href="#" title="Комментарий">
                        <div class="like_button_icon"></div>
                        <div class="like_button_label"></div>
                        <div class="like_button_count">15</div>
                        <span class="blind_label">Комментарий</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</article>
