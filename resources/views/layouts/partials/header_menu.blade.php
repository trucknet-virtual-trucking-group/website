<div id="header_menu_wrap">
    <ul id="header_menu" class="clearfix">
        <li>
            <a href="{{ route('news') }}" class="{{ Request::routeIs('news') ? 'active' : '' }}" onclick="nav.go(this); return false">Новости</a>
        </li>
        <li>
            <a href="{{ route('chat') }}" class="{{ Request::routeIs('chat') ? 'active' : '' }}" onclick="nav.go(this); return false">Общий чат</a>
        </li>
        <li>
            <a href="{{ route('vtc') }}" class="{{ Request::routeIs('vtc') ? 'active' : '' }}" onclick="nav.go(this); return false">Компании</a>
        </li>
        <li>
            <a href="{{ route('users') }}" class="{{ Request::routeIs('users') ? 'active' : '' }}" onclick="nav.go(this); return false">Пользователи</a>
        </li>
        <li>
            <a href="{{ route('contests') }}" class="{{ Request::routeIs('contests') ? 'active' : '' }}" onclick="nav.go(this); return false">Конкурсы</a>
        </li>
        <li>
            <a href="{{ route('tasks') }}" class="{{ Request::routeIs('tasks') ? 'active' : '' }}" onclick="nav.go(this); return false">Задания</a>
        </li>
        <li>
            <a href="{{ route('files') }}" class="{{ Request::routeIs('files') ? 'active' : '' }}" onclick="nav.go(this); return false">Файлы</a>
        </li>
        <li>
            <a href="{{ route('contract') }}" class="{{ Request::routeIs('contract') ? 'active' : '' }}" onclick="nav.go(this); return false">Контракты</a>
        </li>
        <li>
            <a href="{{ route('plugin') }}" class="{{ Request::routeIs('plugin') ? 'active' : '' }}" onclick="nav.go(this); return false">Плагин</a>
        </li>
        <li>
            <a href="{{ route('forum') }}" class="{{ Request::routeIs('forum') ? 'active' : '' }}" onclick="nav.go(this); return false">Форум</a>
        </li>
        <li>
            <a href="{{ route('support') }}" class="{{ Request::routeIs('support') ? 'active' : '' }}" onclick="nav.go(this); return false">Помощь</a>
        </li>
    </ul>
</div>

@if (Auth::check())

@endif
