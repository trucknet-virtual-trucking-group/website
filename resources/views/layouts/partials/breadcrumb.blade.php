<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Главная страница</h4>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Вы просматриваете главную страницу сайта</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <button type="button" class="btn btn-info btn-rounded"><i class="far fa-heart"></i> Премиум
                    аккаунт
                </button>
            </div>
        </div>
    </div>
</div>
