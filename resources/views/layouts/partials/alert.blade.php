@if (Session::has('alert.message'))
    <script type="text/javascript">
        Notifications.show(
            {
                pic: "/images/notifications/error.png",
                title: "{{ Session::get('alert.title') }}",
                content: "{!! Session::get('alert.message') !!}"
            }
        );
    </script>
@endif
