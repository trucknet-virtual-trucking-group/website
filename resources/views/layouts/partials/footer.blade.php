<div id="footer_wrap">
    <div id="footer">
        <div class="clearfix">
            <div class="fl_l">
                <div class="copyright">© 2020 «vtcmanager.ru»</div>
            </div>

            <div class="fl_r menu">
                <a href="#" class="underline">О проекте</a>
                <a href="#" class="underline">Новости проекта</a>
            </div>
        </div>
    </div>
</div>
