<div class="profile_post_wrap br3px">
    <div class="profile_submit_post_box br3px">
        <a href="#" class="profile_post_field_user_link">
            <img class="profile_post_field_user_image" src="{{ Auth::user()->getSmallAvatar() }}" alt="avatar"/>
        </a>

        <div class="profile_post_field_wrap">
            <div class="profile_submit_post_field dark submit_post_inited" contenteditable="true" role="textbox"
                 aria-multiline="true" onkeyup="Wall.postChanged()" onfocus="Wall.showEditPost()"></div>
            <div class="placeholder user_select_none">
                <div class="ph_input">
                    <div class="ph_content">Что у Вас нового?</div>
                </div>
            </div>
        </div>

        <div class="profile_submit_post clearfix">
            <div class="add_post_button_wrap fl_r">
                <div id="add_wall" class="green_button user_select_none" onclick="Wall.submit(); return false;">
                    Опубликовать
                </div>
            </div>

            <div class="add_media">
                <div class="media_selector clearfix">
                    <a class="ms_item ms_item_photo" tabindex="0" data-title="Фотография" aria-label="Фотография" role="link">
                        <span class="blind_label">Фотография</span>
                    </a>
                    <a class="ms_item ms_item_audio" tabindex="0" data-title="Аудиозапись" aria-label="Аудиозапись" role="link">
                        <span class="blind_label">Аудиозапись</span>
                    </a>
                </div>
            </div>
        </div>

        @if(!Auth::user()->wallSuggest()->first())
            <div class="suggest" onclick="Wall.hideSuggest(); return false;">
                <div class="suggest_close"></div>
                <div class="suggest_link">
                    <div class="suggest_image"></div>
                    <div class="suggest_info">
                        Напишите свою первую интересную новость, и получите бонус.
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
