@php
$posts = isset($walls) ? $walls : [];
@endphp

@if(count($posts))
    <div class="feed_list">
        @foreach($posts as $wall)
            @include('layouts.partials.wall', $wall)
        @endforeach

        <div class="feed_preloader">
            <span class="upload upload_feed"><span></span><span></span><span></span></span>
        </div>
        <input type="hidden" id="current_user_id" value="{{ Request::routeIs('news') ? '0' : $user->id  }}" />
    </div>
@else
    <div class="no_posts_wrap br3px">
        <div class="no_posts_block br3px">
            <div class="no_posts_cover"></div>
            {{ $text }}
        </div>
    </div>
@endif

@push('main_scripts')
    @modifiedScript
    <script>
        var inProgress = false;
        var startFrom = 2;
        var current_page = $('#current_page');
        var user_id = $('#current_user_id').val();
        var is = !(current_page.val() === 'user.profile' && user_id === '0');

        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() >= $(document).height() - 200 && !inProgress && is && $('.feed_list_item').length > 14) {
                if (current_page.val() === 'user.profile') {
                    ajaxRequest('/wall', 'GET', {
                        page: startFrom,
                        user_id: user_id
                    }, {
                        start: function () {
                            inProgress = true;
                            $('.feed_list .feed_preloader').show();
                        },
                        finish: function () {
                            $('.feed_list .feed_preloader').hide();
                        },
                        success: function (result, msg) {
                            if (result.html.length > 0) {
                                inProgress = false;
                                startFrom += 1;
                                $('.feed_list .feed_preloader').before(result.html);
                            }
                        }
                    });
                } else {
                    ajaxRequest('/wall', 'GET', {
                        page: startFrom,
                    }, {
                        start: function () {
                            inProgress = true;
                            $('.feed_list .feed_preloader').show();
                        },
                        finish: function () {
                            $('.feed_list .feed_preloader').hide();
                        },
                        success: function (result, msg) {
                            if (result.html.length > 0) {
                                inProgress = false;
                                startFrom += 1;
                                $('.feed_list .feed_preloader').before(result.html);
                            }
                        }
                    });
                }
            }
        });
    </script>
    @endmodifiedScript
@endpush
