<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ mix('images/favicon.ico') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {!! SEO::generate() !!}

    <link href="{{ mix('css/common.css') }}" rel="stylesheet">
    @stack('stylesheets')

    <script type="text/javascript">
        var iPopular = {
            page_name: "<?php echo Request::route()->getName()?>",
            section: 0
        };

        var user = {
          user_id: {{ Auth::user()->id ?? 0 }}
        };

        var APP_URL = "{{ config('app.url') }}";

        @stack('routes')
    </script>
</head>
<body>
<div id="header_load"></div>
<div id="loading">
    <div id="load"></div>
</div>
<div id="page">
    <div id="black_bg"></div>
    <div class="notifications_wrap" id="notifications_wrap"></div>

    <div class="banner" id="banner-vkonline" style="display: none">
        <div class="banner__wrapper">
            <p class="banner__desktop-text">
                Привет Николай! У тебя в профиле не указаны "Дата рождения, интересы TruckersMP, а так-же не загружены фотография и обложка, сделай это и получи бонус.
            </p>
            <a class="banner__desktop-link" href="#">Редактировать профиль</a>
            <div class="closed_button trns2s"></div>
        </div>
    </div>

    @if (Request::route()->getName() !== 'index')
        @include('layouts.partials.header')
        @include('layouts.partials.header_menu')
        @include('layouts.partials.nav')
    @endif

    @if (Request::route()->getName() !== 'index')
    <div id="content">
        @yield('content')
    </div>
    @else
        @yield('content')
    @endif

    @if (Request::route()->getName() !== 'index')
        @include('layouts.partials.footer')
    @endif
    <input type="hidden" id="current_page" value="<?php echo Request::route()->getName()?>" />
</div>

<script src="{{ mix('js/vendor.min.js') }}"></script>
<script src="{{ mix('js/common.js') }}"></script>
@stack('scripts')

<script id="main_scripts" type="text/javascript">
    var page_name = '<?php echo Request::route()->getName()?>';

    @stack('main_scripts')

    if (ajax_nav) {
        stManager.load('{{ url(mix('css/common.css')) }}', 'css');
        @stack('st_manager')
    }

    @if(!Auth::guest() && !Auth::user()->login)
    completion.show();
    @endif
</script>
@include('layouts.partials.alert')
</body>
</html>
