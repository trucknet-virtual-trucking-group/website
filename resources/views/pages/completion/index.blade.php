@extends('app')

@section('content')
    <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url({{ url('/images/home/background.png') }}) no-repeat center center;">
        <div class="auth-box auth-box-completion">
            <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body wizard-content">
                        <h4 class="card-title">Завершения регистрации</h4>
                        <h6 class="card-subtitle">Чтобы завершить регистрацию на сайте, заполните форму.</h6>
                        <form action="#" class="validation-wizard wizard-circle">
                            <h6>Этап 1</h6>
                            <section>
                                <div class="form-group">
                                    <label for="rules">Правила использования сервиса</label>
                                    <textarea id="rules" class="form-control" rows="3" style="margin-top: 0; margin-bottom: 0; height: 140px;" disabled>В будущем будут правила сервиса.</textarea>
                                </div>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="rules_accepted" id="rules_accepted" value="1">
                                    <label class="custom-control-label" for="rules_accepted">Я ознакомлен и соглашаюсь с правилами использования сервиса</label>
                                </div>
                            </section>
                            <h6>Этап 2</h6>
                            <section>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="firstName">
                                                Имя: <span class="danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Иван"/>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="lastName">
                                                Фамилия: <span class="danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Иванов">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="dateOfBirth">
                                                Дата рождения: <span class="danger">*</span>
                                            </label>
                                            <div class="input-group">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="icon-calender"></i></span>
                                                </div>
                                                <input type="text" class="form-control" id="datepicker-autoclose" name="dateOfBirth" placeholder="mm/dd/yyyy">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="country">Страна:</label>
                                            <select class="select2-ajax-country form-control required select2-hidden-accessible" id="country" name="country" style="width: 100%"></select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="city">Город:</label>
                                            <select class="select2-ajax-city form-control required select2-hidden-accessible" id="city" name="city" style="width: 100%"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Ваш пол:</label>
                                            <div class="c-inputs-stacked">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="gender_men" name="gender" required class="custom-control-input" value="1">
                                                    <label class="custom-control-label" for="gender_men">Мужской</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="gender_women" name="gender" class="custom-control-input" value="2">
                                                    <label class="custom-control-label" for="gender_women">Женский</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <h6>Этап 3</h6>
                            <section>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="vk">Страница ВКонтакте:</label>
                                            <input type="url" class="form-control" id="vk_id" name="vk_id" placeholder="https://vk.com/durov">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="youtube">Youtube Канал:</label>
                                            <input type="url" class="form-control" id="youtube" name="youtube" placeholder="https://youtube.com/channel/trucknet">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="instagram">Instagram:</label>
                                            <input type="url" class="form-control" id="instagram" name="instagram" placeholder="https://instagram.com/durov">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="twitch">Twitch:</label>
                                            <input type="url" class="form-control" id="twitch" name="twitch" placeholder="https://twitch.tv/csgomc_ru">
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <h6>Этап 4</h6>
                            <section>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            @include('pages.completion.partials.truckersMpEtsServers', ['servers' => $servers])
                                        </div>
                                        <div class="form-group">
                                            @include('pages.completion.partials.trucks_ets', ['trucks' => $trucks])
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            @include('pages.completion.partials.truckersMpAtsServers', ['servers' => $servers])
                                        </div>
                                        <div class="form-group">
                                            @include('pages.completion.partials.trucks_ats', ['trucks' => $trucks])
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <h6>Этап 5</h6>
                            <section>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">Email:</label>
                                            <input type="email" class="form-control required" id="email" name="email" placeholder="ivan@mail.ru">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="password">
                                                Пароль: <span class="danger">*</span>
                                            </label>
                                            <input type="password" class="form-control required" id="password" name="password" placeholder="******">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="confirmPassword">Подтверждение пароля:</label>
                                            <input type="password" class="form-control required" id="password_confirmation" name="password_confirmation" placeholder="******">
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@push('stylesheets')
    <link href="{{ asset('css/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-steps/jquery.steps.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery-steps/steps.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2/select2.min.css') }}" rel="stylesheet">
@endpush

@push('scripts')
    <script src="{{ asset('js/toastr/toastr.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery-steps/jquery.steps.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery-validation/localization/messages_ru.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap-datepicker/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap-datepicker/locales/bootstrap-datepicker.ru.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/select2/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/select2/ru.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/completion/completion.js') }}" type="text/javascript"></script>
@endpush
