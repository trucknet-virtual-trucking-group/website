<label for="truckers_mp_ats_truck">Предпочтительный тягач в ATS:</label>
<select class="custom-select form-control" id="ats_preferred_truck" name="ats_preferred_truck">
    <option value="Не играю">Не играю</option>
    @foreach($trucks as $truck)
        @if($truck->game_id == 2)
            <option value="{{ $truck->id }}">{{ $truck->name }}</option>
        @endif
    @endforeach
</select>
