<label for="truckers_mp_ets_truck">Предпочтительный тягач в ETS2:</label>
<select class="custom-select form-control" id="ets_preferred_truck" name="ets_preferred_truck">
    <option value="Не играю">Не играю</option>
    @foreach($trucks as $truck)
        @if($truck->game_id == 1)
            <option value="{{ $truck->id }}">{{ $truck->name }}</option>
        @endif
    @endforeach
</select>
