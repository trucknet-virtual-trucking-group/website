<label for="truckers_mp_ets">Предпочтительный сервер MP в ATS:</label>
<select class="custom-select form-control" id="ets_preferred_server" name="ets_preferred_server">
    <option value="0">Не играю</option>
    @foreach($servers as $server)
        @if($server->game_id == 2)
            <option value="{{ $server->id }}">{{ $server->name }}</option>
        @endif
    @endforeach
</select>
