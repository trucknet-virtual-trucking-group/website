<label for="truckers_mp_ats">Предпочтительный сервер MP в ETS2:</label>
<select class="custom-select form-control" id="ats_preferred_server" name="ats_preferred_server">
    <option value="0">Не играю</option>
    @foreach($servers as $server)
        @if($server->game_id == 1)
            <option value="{{ $server->id }}">{{ $server->name }}</option>
        @endif
    @endforeach
</select>
