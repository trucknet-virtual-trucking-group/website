@extends('app')

@section('content')
    <div class="universal_block_wrap">
        <div class="universal_block">
            <div class="title">
                Euro Truck Simulator 2 & American Truck Simulator
                <a href="#" class="link fl_r">Показать все
                    <svg width="4" height="8" xmlns="http://www.w3.org/2000/svg">
                        <path d="M.7 7.2L3.3 4 .7.8" stroke="#385169" stroke-width="1.3" fill="none" fill-rule="evenodd"
                              stroke-linecap="round" stroke-linejoin="round"></path>
                    </svg>
                </a>
            </div>
            <div class="universal_block_inner">
                <div class="news_list">
                    <div class="news_list_item">
                        <div class="news_list_item_item">
                            <div class="news_list_item_wrapper">
                                <div class="best_bage">
                                    Популярное
                                </div>
                                <div class="news_list_item_img">
                                    <div class="news_list_item_img_wrapper">
                                        <div class="news_list_item_image" style="background: url(https://sun1-96.userapi.com/6iRRKaOgLSHv9vCHiDlBYU5abCQWmrm0crsI1g/n-it452e4sM.jpg) center center / cover no-repeat rgb(237, 238, 240);">

                                        </div>
                                    </div>
                                </div>
                                <div class="news_list_item_info">
                                    <div class="news_list_item_title">
                                        Релиз FH Tuning Pack
                                    </div>
                                    <div class="news_list_item_description">
                                        <div class="news_list_item_description_sub">
                                            Нам нравится наблюдать за тем, как наше сообщество настраивает свои грузовики, и сколько времени водителям требуется, чтобы убедиться...
                                        </div>
                                        <div class="news_list_item_type">
                                            <div class="news_list_item_type_title">
                                                Euro Truck Simulator 2
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="news_list_item">
                        <div class="news_list_item_item">
                            <div class="news_list_item_wrapper">
                                <div class="news_list_item_img">
                                    <div class="news_list_item_img_wrapper">
                                        <div class="news_list_item_image" style="background: url(https://sun9-24.userapi.com/c854124/v854124393/235898/rNL6o_NYiNw.jpg) center center / cover no-repeat rgb(237, 238, 240);">

                                        </div>
                                    </div>
                                </div>
                                <div class="news_list_item_info">
                                    <div class="news_list_item_title">
                                        Ивент "Операция мост Генуя”
                                    </div>
                                    <div class="news_list_item_description">
                                        <div class="news_list_item_description_sub">
                                            На прошлой неделе мы опубликовали тизерное изображение строительной площадки, работающей в неизвестном месте в Euro...
                                        </div>
                                        <div class="news_list_item_type">
                                            <div class="news_list_item_type_title">
                                                Euro Truck Simulator 2
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="news_list_item">
                        <div class="news_list_item_item">
                            <div class="news_list_item_wrapper">
                                <div class="news_list_item_img">
                                    <div class="news_list_item_img_wrapper">
                                        <div class="news_list_item_image" style="background: url(https://sun1-24.userapi.com/GaQJzJ14yF8iMeyh9DbTTW0zXH6tJnAZC5JG4Q/3rew1K3DWpU.jpg) center center / cover no-repeat rgb(237, 238, 240);">

                                        </div>
                                    </div>
                                </div>
                                <div class="news_list_item_info">
                                    <div class="news_list_item_title">
                                        DLC Айдахо: Sandpoint to Lewiston Gameplay
                                    </div>
                                    <div class="news_list_item_description">
                                        <div class="news_list_item_description_sub">
                                            Пришло время ближе познакомиться с нашим предстоящим DLC для American Truck Simulator, Айдахо. Здесь...
                                        </div>
                                        <div class="news_list_item_type">
                                            <div class="news_list_item_type_title">
                                                American Truck Simulator
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="news_list_item">
                        <div class="news_list_item_item">
                            <div class="news_list_item_wrapper">
                                <div class="news_list_item_img">
                                    <div class="news_list_item_img_wrapper">
                                        <div class="news_list_item_image" style="background: url(https://sun9-25.userapi.com/c854532/v854532635/22d676/AEC0Aod7Eak.jpg) center center / cover no-repeat rgb(237, 238, 240);">

                                        </div>
                                    </div>
                                </div>
                                <div class="news_list_item_info">
                                    <div class="news_list_item_title">
                                        TRUCKERSMP GENERAL SURVEY
                                    </div>
                                    <div class="news_list_item_description">
                                        <div class="news_list_item_description_sub">
                                            Greetings truckers! In order to improve, measuring is key. It's been a while since we've done a survey so now we're back with...
                                        </div>
                                        <div class="news_list_item_type">
                                            <div class="news_list_item_type_title">
                                                TruckersMP
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="feed_wrap">
        <div class="clearfix">
            <div id="content_left" class="fl_r">
                <div class="r_menu_wrap br3px">
                    <div class="r_menu_inner br3px">
                        <a href="{{ route('news') }}" onclick="nav.go(this); return false" class="active trns15s user_select_none">Все новости</a>
                    </div>
                </div>

                <div class="universal_block_wrap" id="hashtags">
                    <div class="universal_block">
                        <div class="title">
                            Популярные хэштеги
                        </div>
                        <div class="universal_block_inner">
                            <div class="hashtags_list">
                                <a href="#" onclick="nav.go(this); return false" class="trns15s user_select_none">#atsmp</a>
                                <a href="#" onclick="nav.go(this); return false" class="trns15s user_select_none">#маршрут</a>
                                <a href="#" onclick="nav.go(this); return false" class="trns15s user_select_none">#scssoftware</a>
                                <a href="#" onclick="nav.go(this); return false" class="trns15s user_select_none">#ets2mp</a>
                                <a href="#" onclick="nav.go(this); return false" class="trns15s user_select_none">#truckersmp</a>
                                <a href="#" onclick="nav.go(this); return false" class="trns15s user_select_none">#vtcmanager</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="universal_block_wrap" id="hashtags">
                    <div class="universal_block">
                        <div class="title">
                            Возможные друзья
                        </div>
                        <div class="universal_block_inner">
                            <ul class="friends_list">
                                <li class="friend fl_l no_margin_bottom">
                                    <div class="avatar">
                                        <a href="#">
                                            <img
                                                src="https://sun9-32.userapi.com/c850608/v850608306/1334ca/f0ZPOOWrQec.jpg?ava=1"
                                                alt="Вася Ключник" title="Вася Ключник">
                                        </a>
                                    </div>
                                    <div class="name">
                                        <a href="#">Вася</a>
                                    </div>
                                </li>
                                <li class="friend fl_l no_margin_bottom">
                                    <div class="avatar">
                                        <a href="#">
                                            <img
                                                src="https://sun9-32.userapi.com/c850608/v850608306/1334ca/f0ZPOOWrQec.jpg?ava=1"
                                                alt="Вася Ключник" title="Вася Ключник">
                                        </a>
                                    </div>
                                    <div class="name">
                                        <a href="#">Вася</a>
                                    </div>
                                </li>
                                <li class="friend fl_l no_margin_bottom">
                                    <div class="avatar">
                                        <a href="#">
                                            <img
                                                src="https://sun9-32.userapi.com/c850608/v850608306/1334ca/f0ZPOOWrQec.jpg?ava=1"
                                                alt="Вася Ключник" title="Вася Ключник">
                                        </a>
                                    </div>
                                    <div class="name">
                                        <a href="#">Вася</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="bottom_button trns2s user_select_none">
                            Показать всех
                        </div>
                    </div>
                </div>
            </div>
            <div id="content_right" class="fl_l">
                @include('layouts.postAdd')
                @include('layouts.wall', ['text' => 'Нет ни одной новости.'])
            </div>
        </div>
    </div>

@endsection
