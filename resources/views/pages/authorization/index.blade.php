@extends('app')

@section('content')
    <div id="ft_body_bg">
        <div id="ft_body_bg_inner">
            <div id="ft_logo_company_name">VTCmanager</div>
            <div id="ft_logo_slogan">Набор мощных инструментов</div>
            <div id="ft_content">
                <div id="ft_content_description">
                    <h1 id="ft_content_description_main_text">
                        Добро пожаловать в <a href="#" title="VTCMANAGER" target="_blank">VTCmanager</a>
                    </h1>
                    <div id="ft_fast_order_description">
                        Это набор мощных инструментов по созданию и управлению виртуальными компаниями в
                        EuroTruckSimulator2 & AmericanTruckSimulator.
                        <br/> <br/>
                        Присоединяйтесь к нам, и станьте частью огромного сообщества.
                    </div>
                    <div id="ft_fast_order_arrow"></div>
                    <div id="ft_fast_order_button" class="user_select_none" onclick="users.wnd_steam()">РЕГИСТРАЦИЯ
                    </div>
                </div>
                <div id="ft_content_auth">
                    <div id="ft_auth_title">
                        Войдите в свой аккаунт
                    </div>
                    <div id="ft_auth_fields">
                        <div id="ft_auth_error"></div>
                        <input type="text" id="ulogin" iplaceholder="Логин" class="placeholder"/>
                        <span id="ft_last_password_wrap" onclick="users.go_restore(); return false">
                            <span id="ft_last_password_wrap_button"
                                  style="border-top: 1px solid rgb(54, 73, 94); border-bottom: 1px solid rgb(54, 73, 94);">
                                <span id="ft_last_password_wrap_button_text">Забыли?</span>
                            </span>
                        </span>
                        <input type="password" id="upassword" iplaceholder="Пароль" class="placeholder"/>
                    </div>
                    <div id="ft_auth_buttons">
                        <div class="user_select_none" id="ft_auth_buttons_auth" onclick="users.auth()">Войти</div>
                        <div class="user_select_none" id="ft_auth_buttons_reg" onclick="users.wnd_steam()">Регистрация
                        </div>
                    </div>
                    <div id="ft_auth_social">
                        <a class="ft_auth_social_item vk" href="{{ route('auth.social', 'vkontakte') }}">
                            <img alt="VK" src="{{ asset('images/auth/vk.svg') }}">
                        </a>
                        <a class="ft_auth_social_item ok" href="{{ route('auth.social', 'odnoklassniki') }}">
                            <img alt="Odnoklassniki" src="{{ asset('images/auth/odnoklassniki.svg') }}">
                        </a>
                        <a class="ft_auth_social_item yandex" href="{{ route('auth.social', 'yandex') }}">
                            <img alt="Yandex" src="{{ asset('images/auth/yandex.svg') }}">
                        </a>
                        <a class="ft_auth_social_item google" href="{{ route('auth.social', 'Google') }}">
                            <img alt="Google" src="{{ asset('images/auth/google.svg') }}">
                        </a>
                        <a class="ft_auth_social_item mail" href="{{ route('auth.social', 'mailru') }}">
                            <img alt="Mail.ru" src="{{ asset('images/auth/mail.svg') }}">
                        </a>
                    </div>
                    <div id="ft_auth_rules">
                        Пользуясь сайтом, Вы соглашаетесь с <a href="#" target="_blank">правилами</a>
                    </div>
                </div>
            </div>
            <div id="ft_auth_fast_description">
                <ul>
                    <li>
                        <h2>
                            <a href="#" title="Компании" target="_blank">Компании</a></h2>
                        <h3>С нами уже 1000 водителей в<br>более чем 250 компаниях. Ежедневно организуется более 100
                            конвоев.</h3>
                    </li>
                    <li>
                        <h2>
                            <a href="#" title="Основателями" target="_blank">Основателям</a></h2>
                        <h3>У нас есть огромный выбор инструментов для управления и организации вашей ВТК.</h3>
                    </li>
                    <li>
                        <h2>
                            <a href="#" title="Бесплатная накрутка подписчиков" target="_blank">Достижения</a>
                        </h2>
                        <h3>Повышайте рейтинг компании и получайте достижения. Станьте достойным конкурентом на
                            рынке.</h3>
                    </li>
                    <li>
                        <h2>
                            <a href="#" title="Бесплатная накрутка опросов" target="_blank">Срочные заказы</a></h2>
                        <h3>Успейте ухватить "спец груз" в разделе "Срочные заказы" и доставить его вовремя.</h3>
                    </li>
                </ul>
            </div>
        </div>
        <div id="ft_footer">
            <ul>
                <li>
                    <a href="#" target="_blank">О проекте</a>
                </li>
                <li>
                    <a href="#" target="_blank">Правила</a>
                </li>
                <li>
                    <a href="#" target="_blank">Контакты</a>
                </li>
                <li>
                    <a href="#" target="_blank">Новости</a>
                </li>
            </ul>
            <div id="live_counter"></div>
            <div id="ft_footer_copyright">VTCmanager © 2020</div>
        </div>
    </div>
    <style type="text/css">
        body {
            background: #2e4155;
        }
    </style>
@endsection

@push('main_scripts')
    @modifiedScript
    <script>
        $('#upassword').focus(function () {
            $('#ft_last_password_wrap_button').css({borderTop: '1px solid #46aef4', borderBottom: '1px solid #46aef4'});
        });

        $('#upassword').focusout(function () {
            $('#ft_last_password_wrap_button').css({borderTop: '1px solid #36495e', borderBottom: '1px solid #36495e'});
        });

        $('#ulogin').attr('iplaceholder', 'Логин');
        $('#upassword').attr('iplaceholder', 'Пароль');

        iplaceholder('#ulogin');
        iplaceholder('#upassword');

        $('#ulogin, #upassword').keydown(function (event) {
            var keyCode = event.which;
            if (keyCode === 13) {
                users.auth();
                return false;
            }
        });
    </script>
    @endmodifiedScript
@endpush
