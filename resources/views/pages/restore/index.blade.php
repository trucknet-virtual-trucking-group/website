@extends('app')

@section('content')
<div id="pages">
    <div class="block_wrap">
        <div class="title">Востановление доступа к аккаунту</div>
        <div class="block">
            <div id="restore">
                <div id="restore_inner">
                    @if(!isset($token))
                    <div id="restore_inner_help">
                        Пожалуйста выберите для вас удобный способ восстановления доступа к аккаунту ниже.
                    </div>
                    <div id="form_restore">
                        <div id="form_restore_error"></div>
                        <ul class="restore_select" id="uLogin" data-ulogin="display=buttons;callback=restoreHandle">
                            <li>
                                <div class="restore_select_block vk user_select_none" data-uloginbutton="vkontakte">
                                    <div class="icon socials vk"><i></i></div>
                                    <div class="name">Восстановить через страницу Вконтакте</div>
                                    <div class="desc">Если вы привязывали вашу страницу к профилю.</div>
                                </div>
                            </li>
                            <li>
                                <div class="restore_select_block steam user_select_none" data-uloginbutton="steam">
                                    <div class="icon socials steam"><i></i></div>
                                    <div class="name">Восстановить через Steam</div>
                                    <div class="desc">Быстрый способ восстановить.</div>
                                </div>
                            </li>
                            <li>
                                <div class="restore_select_block mail user_select_none" onclick="$('#form_mail').toggle()">
                                    <div class="icon socials mail"><i></i></div>
                                    <div class="name">Восстановить через E-mail</div>
                                    <div class="desc">Восстановление по Логину или E-mail</div>
                                </div>
                            </li>
                        </ul>

                        <div id="form_mail">
                            <div id="form_mail_error"></div>
                            <div class="field_title">Логин или e-mail:</div>
                            <div class="field_input">
                                <input type="text" id="login">
                                <div class="error login"></div>
                                <div class="field_help">
                                    Введите почту, которая была привязана к аккаунту. Обратите внимание, что почта должна быть подтверждена.
                                </div>
                            </div>
                            <div id="restore_submit_button" class="green_button user_select_none" onclick="restore.mail(); return false;">Отправить</div>
                        </div>
                    </div>
                    @else
                    <div id="form_restore">
                        <div id="form_restore_error"></div>
                        <div class="field_title">Новый пароль:</div>
                        <div class="field_input">
                            <input type="password" id="restore_new_password" maxlength="20" iplaceholder="******" class="placeholder">
                            <div class="error new_password"></div>
                        </div>
                        <div class="field_title">Повторите пароль:</div>
                        <div class="field_input">
                            <input type="password" id="restore_new_password_repeat" maxlength="20" iplaceholder="******" class="placeholder">
                            <div class="error new_password_repeat"></div>
                        </div>
                        <div id="restore_submit_button" class="green_button user_select_none" onclick="restore.submit(); return false;">Изменить пароль и авторизоваться</div>
                        <input type="hidden" id="unique_token_restore" value="{{ $token }}" />
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('main_scripts')
    @modifiedScript
    <script>
        iplaceholder('#restore_new_password');
        iplaceholder('#restore_new_password_repeat');

        $('#restore_new_password, #restore_new_password_repeat').keydown(function (event) {
            var keyCode = event.which;
            if (keyCode === 13) {
               // users.auth();
                return false;
            }
        });
    </script>
    @endmodifiedScript
@endpush
