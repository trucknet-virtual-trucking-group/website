@extends('app')

@section('content')
    <div class="not_found br3px">
        <div class="not_found_block br3px">
            <div class="not_found_cover"></div>
            Раздел <b>Конкурсов</b> находится в разработке.....
        </div>
    </div>
@endsection
