@extends('app')

@section('content')
    <div id="profile">
        <div class="profile_block br3px">
            @include('pages.profile.partials.cover')
        </div>
        <div class="profile_content">
            <div class="clearfix">
                <div class="profile_content_left fl_l">
                    @include('pages.profile.partials.info')
                    @include('pages.profile.partials.friends')
                    @include('pages.profile.partials.albums')
                </div>

                <div class="profile_content_right fl_r">
                    @include('pages.profile.partials.photos')

                    @if (!Auth::guest() && Auth::user()->id === $user->id)
                    @include('layouts.postAdd')
                    @endif

                    @include('pages.profile.partials.wall')
                </div>
            </div>
        </div>
    </div>
@endsection
