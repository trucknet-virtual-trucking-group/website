
<div class="profile_cover" style="background-image: url({{ $user->getCover() }})">
    <div class="profile_cover_upload">
        @if($user->id === Auth::user()->id)
        <form action="{{ route('user.profile.upload.cover') }}" enctype="multipart/form-data" method="post" target="iframe_load_file_upload_cover">
            <label for="load_file_type_upload_cover" class="attach">
                <svg viewBox="0 0 16 16">
                    <path d="M13 11.42V5h-2.281a1.465 1.465 0 0 0-.283-.626c-.23-.299-.465-.464-1.157-.736L7.556 3H13a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V7.873l.821.304.788 2.13c.091.232.225.439.391.616v1.464l.598-.743a1.544 1.544 0 0 1 2.442.046l.816 1.097 2.511-2.851a1.544 1.544 0 0 1 2.314-.004L13 11.42zM5.072 3l.52 1.406L7.199 5l1.387.513-2.992 1.108-1.108 2.992-1.107-2.992L3 6.48l-2-.74-.615-.228 2.993-1.107 1.107-2.993L5.072 3z" fill-rule="evenodd"></path>
                </svg> Загрузить обложку
            </label>
            <label class="attach" id="remove_cover" onclick="settings.remove_cover();" @if (!$user->cover_id) style="display: none" @endif>
                <svg class="svg-ic svg-ico_trash_16" viewBox="0 0 16 16">
                    <path d="M12.626 13.11a2 2 0 0 1-1.999 1.877l-5.261-.005a2 2 0 0 1-1.995-1.883l-.399-6.814h10.073l-.419 6.825zM6.541 3.008V3c0-.552.654-1 1.459-1s1.459.448 1.459 1v.008h3.41a.987.987 0 0 1 .987.987.987.987 0 0 1-.987.987H3.131a.987.987 0 0 1-.987-.987.987.987 0 0 1 .987-.987h3.41z" fill-rule="evenodd" class="svg-fill"></path>
                </svg> Убрать обложку
            </label>
            <div class="none">
                <input id="load_file_submit_button_upload_cover" type="submit">
                <input name="file" type="file" id="load_file_type_upload_cover" class="file_type" onchange="settings.uploadCover(); return false;">
                @csrf
                <iframe name="iframe_load_file_upload_cover" src="javascript://"></iframe>
            </div>
        </form>
        @endif
    </div>
</div>
<div class="profile_cover_info clearfix">
    <div class="profile_cover_counts fl_r">
        <a href="#" class="profile_counter trns2s br3px">
            <div class="count">0</div>
            <div class="label">Постов</div>
        </a>
        <a href="#" class="profile_counter trns2s br3px">
            <div class="count">0</div>
            <div class="label">Комментариев</div>
        </a>
        <a href="#" class="profile_counter trns2s br3px">
            <div class="count">0</div>
            <div class="label">Поставок</div>
        </a>
        <a href="#" class="profile_counter trns2s br3px">
            <div class="count">0</div>
            <div class="label">Доход</div>
        </a>
        <a href="#" class="profile_counter trns2s br3px">
            <div class="count">0</div>
            <div class="label">Нарушений</div>
        </a>
    </div>
    <a href="javascript://" class="profile_cover_image">
        <img src="{{ $user->getSmallAvatar() }}" class="profile_img" alt="avatar"/>
        @if($user->id === Auth::user()->id)
        <div class="upload_preloader br3px">
            <span class="upload upload_white"><span></span><span></span><span></span></span>
        </div>
        <div class="profile_cover_image_upload" id="upload_avatar">
            <form action="{{ route('user.profile.upload.avatar') }}" enctype="multipart/form-data" method="post" target="iframe_load_file_upload_avatar">
                <label for="load_file_type_upload_avatar"></label>
                <div class="none">
                    <input id="load_file_submit_button_upload_avatar" type="submit">
                    <input name="file" type="file" id="load_file_type_upload_avatar" class="file_type" onchange="settings.uploadAvatar(); return false;">
                    @csrf
                    <iframe name="iframe_load_file_upload_avatar" src="javascript://"></iframe>
                </div>
            </form>
        </div>
        @endif
    </a>

    <div class="profile_top">
        <h1 class="profile_name">{{ $user->fullName() }}</h1>
        <div class="profile_status gray">На проекте {{ $user->timeTheProject() }}</div>
    </div>
</div>
