<div class="profile_photos_wrap br3px">
    <div class="profile_photos_block">
        <div class="profile_photos_list">
            <div class="profile_photos_not_found" style="display: none">
                <div class="no_photos_cover trns2s"></div>
                Загрузите свою первую фотографию
            </div>

            <a class="square_photo crisp_image br3px" href="#" aria-label="фотография" style="background-image: url(https://sun9-65.userapi.com/c206720/v206720455/11445c/vYmx3tNYclg.jpg)">
                <span class="blind_label">фотография</span>
            </a>

            <a class="square_photo crisp_image br3px" href="#" aria-label="фотография" style="background-image: url(https://sun9-64.userapi.com/c857528/v857528455/1ebf9f/PzVNt-Zxl6s.jpg)">
                <span class="blind_label">фотография</span>
            </a>

            <a class="square_photo crisp_image br3px" href="#" aria-label="фотография" style="background-image: url(https://sun9-60.userapi.com/c857520/v857520455/1f1b86/O7dASBEMNHE.jpg)">
                <span class="blind_label">фотография</span>
            </a>

            <a class="square_photo crisp_image br3px" href="#" aria-label="фотография" style="background-image: url(https://sun9-12.userapi.com/c857724/v857724644/1e1b92/baOPuvDGtiA.jpg)">
                <span class="blind_label">фотография</span>
            </a>

            <a class="square_photo crisp_image br3px" href="#" aria-label="фотография" style="background-image: url(https://sun9-4.userapi.com/c857328/v857328644/18a145/m2lDvlfwo68.jpg)">
                <span class="blind_label">фотография</span>
            </a>
        </div>
    </div>
</div>
