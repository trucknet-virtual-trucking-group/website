<div class="profile_friends_wrap br3px">

    <div class="universal_block_wrap">
        <div class="universal_block">
            <div class="title">
                Друзья <b>2 друга</b>
            </div>
            <div class="universal_block_inner">
                <ul class="friends_list">
                    <li class="friend fl_l">
                        <div class="avatar">
                            <a href="#">
                                <img
                                    src="https://sun9-32.userapi.com/c850608/v850608306/1334ca/f0ZPOOWrQec.jpg?ava=1"
                                    alt="Вася Ключник" title="Вася Ключник">
                            </a>
                        </div>
                        <div class="name">
                            <a href="#">Вася</a>
                        </div>
                    </li>
                    <li class="friend fl_l">
                        <div class="avatar">
                            <a href="#">
                                <img
                                    src="https://sun9-32.userapi.com/c850608/v850608306/1334ca/f0ZPOOWrQec.jpg?ava=1"
                                    alt="Вася Ключник" title="Вася Ключник">
                            </a>
                        </div>
                        <div class="name">
                            <a href="#">Вася</a>
                        </div>
                    </li>
                    <li class="friend fl_l">
                        <div class="avatar">
                            <a href="#">
                                <img
                                    src="https://sun9-32.userapi.com/c850608/v850608306/1334ca/f0ZPOOWrQec.jpg?ava=1"
                                    alt="Вася Ключник" title="Вася Ключник">
                            </a>
                        </div>
                        <div class="name">
                            <a href="#">Вася</a>
                        </div>
                    </li>
                    <li class="friend fl_l">
                        <div class="avatar">
                            <a href="#">
                                <img
                                    src="https://sun9-32.userapi.com/c850608/v850608306/1334ca/f0ZPOOWrQec.jpg?ava=1"
                                    alt="Вася Ключник" title="Вася Ключник">
                            </a>
                        </div>
                        <div class="name">
                            <a href="#">Вася</a>
                        </div>
                    </li>
                    <li class="friend fl_l">
                        <div class="avatar">
                            <a href="#">
                                <img
                                    src="https://sun9-32.userapi.com/c850608/v850608306/1334ca/f0ZPOOWrQec.jpg?ava=1"
                                    alt="Вася Ключник" title="Вася Ключник">
                            </a>
                        </div>
                        <div class="name">
                            <a href="#">Вася</a>
                        </div>
                    </li>
                    <li class="friend fl_l">
                        <div class="avatar">
                            <a href="#">
                                <img
                                    src="https://sun9-32.userapi.com/c850608/v850608306/1334ca/f0ZPOOWrQec.jpg?ava=1"
                                    alt="Вася Ключник" title="Вася Ключник">
                            </a>
                        </div>
                        <div class="name">
                            <a href="#">Вася</a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="bottom_button trns2s user_select_none">
                Посмотреть всех друзей
            </div>
        </div>
    </div>

    <div class="universal_block_wrap mrtop25">
        <div class="universal_block">
            <div class="title">Друзья онлайн</div>
            <div class="universal_block_inner">
                <ul class="friends_list">
                    <li class="friend fl_l">
                        <div class="avatar">
                            <a href="#">
                                <img
                                    src="https://sun9-32.userapi.com/c850608/v850608306/1334ca/f0ZPOOWrQec.jpg?ava=1"
                                    alt="Вася Ключник" title="Вася Ключник">
                            </a>
                        </div>
                        <div class="name">
                            <a href="#">Вася</a>
                        </div>
                    </li>
                    <li class="friend fl_l">
                        <div class="avatar">
                            <a href="#">
                                <img
                                    src="https://sun9-32.userapi.com/c850608/v850608306/1334ca/f0ZPOOWrQec.jpg?ava=1"
                                    alt="Вася Ключник" title="Вася Ключник">
                            </a>
                        </div>
                        <div class="name">
                            <a href="#">Вася</a>
                        </div>
                    </li>
                    <li class="friend fl_l">
                        <div class="avatar">
                            <a href="#">
                                <img
                                    src="https://sun9-32.userapi.com/c850608/v850608306/1334ca/f0ZPOOWrQec.jpg?ava=1"
                                    alt="Вася Ключник" title="Вася Ключник">
                            </a>
                        </div>
                        <div class="name">
                            <a href="#">Вася</a>
                        </div>
                    </li>
                    <li class="friend fl_l">
                        <div class="avatar">
                            <a href="#">
                                <img
                                    src="https://sun9-32.userapi.com/c850608/v850608306/1334ca/f0ZPOOWrQec.jpg?ava=1"
                                    alt="Вася Ключник" title="Вася Ключник">
                            </a>
                        </div>
                        <div class="name">
                            <a href="#">Вася</a>
                        </div>
                    </li>
                    <li class="friend fl_l">
                        <div class="avatar">
                            <a href="#">
                                <img
                                    src="https://sun9-32.userapi.com/c850608/v850608306/1334ca/f0ZPOOWrQec.jpg?ava=1"
                                    alt="Вася Ключник" title="Вася Ключник">
                            </a>
                        </div>
                        <div class="name">
                            <a href="#">Вася</a>
                        </div>
                    </li>
                    <li class="friend fl_l">
                        <div class="avatar">
                            <a href="#">
                                <img
                                    src="https://sun9-32.userapi.com/c850608/v850608306/1334ca/f0ZPOOWrQec.jpg?ava=1"
                                    alt="Вася Ключник" title="Вася Ключник">
                            </a>
                        </div>
                        <div class="name">
                            <a href="#">Вася</a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="bottom_button trns2s user_select_none">
                Посмотреть всех кто онлайн
            </div>
        </div>
    </div>
</div>
