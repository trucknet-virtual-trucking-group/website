<div class="profile_albums">
    <div class="profile_albums_block">
        <div class="title">Фотоальбомы</div>
        <div class="block_inner">
            <div class="album_row">
                <a href="#" class="album_link">
                    <div class="album_thumb_wrap">
                        <img class="page_album_thumb" src="https://sun9-27.userapi.com/c858236/v858236258/1f8df0/mH3Jtl5hUgI.jpg" alt="фотографии #3">
                    </div>
                    <div class="album_title">
                        <div class="album_size">512</div>
                        <div class="album_title_text" title="фотографии #3">фотографии #3</div>
                        <div class="album_description">фотографии #3</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
