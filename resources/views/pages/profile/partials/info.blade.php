<div class="profile_info_wrap br3px">
    <div class="profile_info_block br3px">
        <div class="profile_info_block_inner br3px">
            <div class="profile_info_box">
                <div class="profile_info_image calendar"></div>
                <div class="profile_info_text">
                    @if($dateOfBirth)
                        {{ $dateOfBirth }}
                    @else
                        Дата рождения не указана.
                    @endif
                </div>
            </div>

            <div class="profile_info_box">
                <div class="profile_info_image location"></div>
                <div class="profile_info_text">
                    @if($user->city)
                    {{ $user->city }}, {{ $user->country }}
                    @else
                        Город не указан.
                    @endif
                </div>
            </div>

            <div class="profile_info_box">
                <div class="profile_info_image travel"></div>
                <div class="profile_info_text">
                    В компании не состоит.
                </div>
            </div>
        </div>
        <div class="bottom_button trns2s user_select_none">
            Показать полную информацию
        </div>
    </div>
</div>
