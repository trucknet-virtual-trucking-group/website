window.onload = function () {
    nav.goBack();
    stManager.init();
};

var wnd = {
    showBox: function (width, params, fn) {
        $('#black_bg').show();

        var template = '\
   <div id="wnd_box_header_left_head">\
    <div id="wnd_box_header_left_head_title">' + params.title + '</div>\
    <div id="wnd_box_header_left_head_closed"><a href="javascript://" onclick="wnd.closeBox()">Закрыть</a></div>\
   </div>\
   <div id="wnd_box_header_left_text">\
    <div id="wnd_box_header_left_text_inner">\
    ' + params.text + '\
    </div>\
   </div>\
  ';

        $('#wnd_box').text() ? '' : $('body').append('<div id="wnd_box"> </div>');

        $('#wnd_box').show().html(template).css({
            position: 'fixed',
            top: ($(window).height() - $('#wnd_box').height()) / 2,
            left: ($(window).width() - width) / 2,
            width: width
        });
        fn ? fn() : '';

        $('#black_bg').click(function () {
            wnd.closeBox();
        });
    },
    autoPositionBox: function (width) {
        $('#wnd_box').css({
            position: 'fixed',
            top: ($(window).height() - $('#wnd_box').height()) / 2,
            left: ($(window).width() - width) / 2,
            width: width
        });
    },
    closeBox: function () {
        $('#black_bg').hide();
        $('#wnd_box').hide();
    }
};

var ajax_nav = '';
var nav = {
    go: function (url, other_url, back, fn) {
        var url = ((other_url) ? other_url : $(url).attr('href'));

        loader.showHead(20);

        $(window).off('scroll');

        $.get(url, function (data) {
            var ajax_nav = 1;
            loader.showHead(100);

            var result = data;

            var title = result.match(/<title>(.*?)<\/title>/i);
            var content = result.match(/<div id="page">([\s\S]*)<\/div>/i);
            var scripts = result.match(/<script id="main_scripts" type="text\/javascript">([\s\S]*)<\/script>/i);
            var scripts_result = scripts ? scripts[1] : '';

            if (content) {
                // закрываем box'ы
                wnd.closeBox();

                // изменяем title
                document.title = title[1].toString();

                // загружаем содержимое
                $('#page').html(content[1].toString());

                if (window.innerHeight < document.body.scrollHeight) {
                    $('body').css('marginRight', '-' + getScrollBarWidth() + 'px');
                } else {
                    $('body').css('marginRight', 0);
                }

                // исполняем скрипты
                if (scripts_result) {
                    eval(scripts_result);
                }

                pageUpScroll();

                if (fn) {
                    fn();
                }

                if (!back) {
                    window.history.ready = true;

                    history.pushState ? history.pushState({}, '', url) : location.hash = url;
                }

                if ($('#nav_value').html()) {
                    $('#nav_value').html(url);
                } else {
                    $('body').append('<div class="none" id="nav_value">' + url + '</div>');
                }

                setTimeout(function () {
                    loader.hideHead();
                }, 400);
            }
        });

        return false;
    },
    setUrl: function (url) {
        window.history.ready = true;

        history.pushState ? history.pushState({}, '', url) : location.hash = url;

        if ($('#nav_value').html()) {
            $('#nav_value').html(url);
        } else {
            $('body').append('<div class="none" id="nav_value">' + url + '</div>');
        }
    },
    goBack: function () {
        if (history.pushState) {
            $(window).bind('popstate', function (e) {
                var window_location_url_h = $('#nav_value').html();

                if (window_location_url_h) {
                    nav.go('', window.location.href, true, '');
                }
            });
        } else {
            var lhref_interval = setInterval(function () {
                var now_url = window.location.href.split('//');
                var real_now_url = now_url[1].replace('soojoo.ru', '').replace(/\/(.*?)\#/gi, '');

                var ajax_url = $('#nav_value').html();

                if (ajax_url) {
                    if (real_now_url != ajax_url) {
                        nav.go('', real_now_url, true, '');

                        clearInterval(lhref_interval);

                        setTimeout(function () {
                            nav.goBack();
                        }, 100);
                    }
                }
            }, 10);
        }
    }
};

function ajaxRequest(url, method, params, functions) {
    ((functions.start) ? functions.start() : '');

    ((method == 'POST') ? $.post : $.get)(url, ((params) ? params : ''), function (data) {
        var result = data;

        ((functions.finish) ? functions.finish() : '');

        ((functions.result) ? functions.result(result) : '');

        if (result.response || result.key) {
            ((functions.success) ? functions.success(result, result.msg) : '');
        } else if (result.error_msg) {
            if (typeof result.field_id !== 'undefined') {
                $('.error.' + result.field_id).html(result.error_msg);

                $('#' + result.field_id).focus();

                var field_id_value = $('#' + result.field_id).val();

                $('#' + result.field_id).val('');
                $('#' + result.field_id).val(field_id_value);
            }

            ((functions.fail) ? functions.fail(result, result.error_msg) : '');
        } else {
            alert('Произошла ошибка. Попробуйте позже.');
        }
    });
}

var loader = {
    showHead: function (percent, next) {
        $('#header_load').show().animate({width: percent + '%'}, 150);
    },
    hideHead: function () {
        $('#header_load').css('width', 0).hide();
    }
};

var users = {
    auth: function () {
        var login = isValueExst($('#ulogin, #auth_login'));
        var password = isValueExst($('#upassword, #auth_password'));

        if (!login) {
            $('#ulogin, #auth_login').focus();
            return false;
        } else if (password.length < 6) {
            $('#upassword, #auth_password').focus();
            return false;
        }

        ajaxRequest('/login', 'POST', {
            login: login,
            password: password,
        }, {
            start: function () {
                $('#ft_auth_error, #header_auth_form_error_msg').hide();
                $('#ft_auth_buttons_auth, #auth_submit_button .button_inner').html('<span class="upload upload_white"><span></span><span></span><span></span></span>');
            },
            finish: function () {
                $('#ft_auth_buttons_auth, #auth_submit_button .button_inner').html('Войти');
            },
            success: function (result, msg) {
                window.location.href = '/';
            },
            fail: function (result, error_msg) {
                $('#ft_auth_error, #header_auth_form_error_msg').show().html(error_msg);
            }
        });
    },
    captcha: function () {
        ajaxRequest('/api/v1/captcha', 'GET', {}, {
            success: function (result, msg) {
                $('#field_captcha_img').html('<img src="' + result.img + '" alt="Captcha" onclick="users.captcha();">');
                $('#captcha_key').val(result.key);
            },
        });
    },
    wnd_steam: function () {
        wnd.showBox(430, {
            title: 'Регистрация',
            text: '<div class="wnd_reg_block">\
                    <div class="wnd_reg_block_description">\
                     Выберите удобную социальную сеть для регистрации\
                    </div>\
                    <div class="wnd_reg_block_form">\
                        <div id="ft_auth_social">\
                           <a class="ft_auth_social_item vk" href="/auth/vkontakte">\
                             <img alt="VK" src="/images/auth/vk.svg">\
                           </a>\
                           <a class="ft_auth_social_item ok" href="/auth/odnoklassniki">\
                           <img alt="Odnoklassniki" src="/images/auth/odnoklassniki.svg">\
                          </a>\
            <a class="ft_auth_social_item yandex" href="/auth/yandex">\
            <img alt="Yandex" src="/images/auth/yandex.svg">\
            </a>\
            <a class="ft_auth_social_item google" href="/auth/Google">\
            <img alt="Google" src="/images/auth/google.svg">\
            </a>\
            <a class="ft_auth_social_item mail" href="/auth/mailru">\
            <img alt="Mail.ru" src="/images/auth/mail.svg">\
            </a>\
            </div>\
                    </div>\
                   </div>'
        });
    },
    redirect: function () {
        $('#steam_redirect').html('<span class="upload upload_white"><span></span><span></span><span></span></span>');
        window.location.href = '/auth/steam';
    },
    go_restore: function() {
        $('#ft_last_password_wrap_button_text').html('<span class="upload upload_black"><span></span><span></span><span></span></span>');
        nav.go('', '/restore');
    }
};

var settings = {
    uploadAvatar: function () {
        $('#load_file_submit_button_upload_avatar').trigger('click');

        $('.upload_preloader').show();

        $('iframe[name="iframe_load_file_upload_avatar"]').on('load', function () {
            try {
                var result = $.parseJSON($(this).contents().text());

                var error_msg = result.error_msg;

                $('.upload_preloader').hide();

                if (result.response == 1) {
                    $('.profile .avatar_wrap img, .profile_cover_image img, .profile_post_field_user_link img').attr('src', result.avatar_src);
                } else if (error_msg) {
                    Notifications.show({
                        'pic': '/images/notifications/error.png',
                        'title': 'Произошла ошибка.',
                        'content': error_msg
                    });
                } else {
                    Notifications.show({
                        'pic': '/images/notifications/error.png',
                        'title': 'Произошла ошибка. Попробуйте позже.',
                        'content': error_msg
                    });
                }
            } catch (e) {
                Notifications.show({
                    'pic': '/images/notifications/error.png',
                    'title': 'Произошла ошибка. Попробуйте позже.',
                    'content': error_msg
                });
            }

            $('iframe[name="iframe_load_file_upload_avatar"]').off();
        });
    },
    uploadCover: function () {
        $('#load_file_submit_button_upload_cover').trigger('click');

        $('iframe[name="iframe_load_file_upload_cover"]').on('load', function () {
            try {
                var result = $.parseJSON($(this).contents().text());

                var error_msg = result.error_msg;

                if (result.response == 1) {
                    $('.profile_cover').css('background-image', 'url(' + result.cover_src + ')');
                    $('#remove_cover').show().html('Убрать обложку');
                } else if (error_msg) {
                    Notifications.show({
                        'pic': '/images/notifications/error.png',
                        'title': 'Произошла ошибка.',
                        'content': error_msg
                    });
                } else {
                    Notifications.show({
                        'pic': '/images/notifications/error.png',
                        'title': 'Произошла ошибка. Попробуйте позже.',
                        'content': error_msg
                    });
                }
            } catch (e) {
                Notifications.show({
                    'pic': '/images/notifications/error.png',
                    'title': 'Произошла ошибка. Попробуйте позже.',
                    'content': error_msg
                });
            }

            $('iframe[name="iframe_load_file_upload_cover"]').off();
        });
    },
    remove_cover: function () {
        ajaxRequest('/profile/delete/cover', 'POST', {}, {
            start: function () {
                $('#remove_cover').show().html('<span class="upload upload_white"><span></span><span></span><span></span></span>');
            },
            success: function (result, msg) {
                $('#remove_cover').hide();
                $('.profile_cover').css('background-image', 'url(/images/cover.png)');
            },
        });
    }
};

var completion = {
    show: function () {
        wnd.showBox(430, {
            title: 'Завершение регистрации',
            text: '<div id="completion_block">\
                    <div id="title_message">\
                       <div id="title_message_inner">\
                         Придумайте <b>логин</b> и <b>пароль</b> он вам понадобится в дальнейшем для <b>авторизации</b> на нашем сайте.\
                       </div>\
                    </div>\
                    <div id="form">\
                        <div id="form_error error"></div>\
                        <div class="field_title">Логин:</div>\
                        <div class="field_input">\
                            <input type="text" id="login" maxlength="20" iplaceholder="Введите логин" class="placeholder">\
                            <div class="error login"></div>\
                        </div>\
                        <div class="field_title">Пароль:</div>\
                        <div class="field_input">\
                            <input type="password" id="password" maxlength="30" iplaceholder="Введите пароль" class="placeholder">\
                            <div class="error password"></div>\
                        </div>\
                        <div id="completion_button" class="green_button user_select_none" onclick="completion.save(); return false">Сохранить</div>\
                    </div>\
               </div>'
        });
        iplaceholder('#login');
        iplaceholder('#password');
    },
    save: function () {
        var login = isValueExst($('#login'));
        var password = isValueExst($('#password'));

        $('.error').html('');
        $('.msg').hide();

        if (!isLogin(login)) {
            $('.error.login').html('Может содержать только латинские символы, некоторые знаки или цифры и не превышать 2-20 символов.');

            $('#login').focus();
            return false;
        } else if (!isPassword(password)) {
            $('.error.password').html('Может содержать только латинские символы, некоторые знаки или цифры и не превышать 6-30 символов.');

            $('#password').focus();
            return false;
        } else {
            ajaxRequest('/profile/completion', 'POST', {
                login: login,
                password: password
            }, {
                start: function () {
                    $('#completion_button').html('<span class="upload upload_white"><span></span><span></span><span></span></span>');
                },
                success: function (result, msg) {
                    wnd.closeBox();
                },
                fail: function (result, error_msg) {
                    $('#completion_button').html('Сохранить');
                }
            });
        }
    }
};

function restoreHandle(token) {
    $('#form_mail').hide();

    ajaxRequest('/restore', 'POST', {
        token: token,
    }, {
        success: function (result, msg) {
            nav.go('', '/restore?token='+ result.token);
        },
        fail: function (result, error_msg) {
            $('#form_restore_error').show().html('<div class="error_msg">' + error_msg + '</div>');
        }
    });
}

var restore = {
    submit: function () {
        var restore_new_password = isValueExst($('#restore_new_password'));
        var restore_new_password_repeat = isValueExst($('#restore_new_password_repeat'));

        $('#form_restore_error').hide();
        $('.error').hide();

        if (!isPassword(restore_new_password)) {
            $('.error.new_password').show().html('Может содержать только латинские символы, некоторые знаки или цифры и не превышать 6-30 символов.');

            $('#restore_new_password').focus();
            return false;
        } else if (restore_new_password != restore_new_password_repeat) {
            $('.error.new_password_repeat').show().html('Пароли не совпадают.');

            $('#restore_new_password_repeat').focus();
            return false;
        } else {
            ajaxRequest('/restore/newpass', 'POST', {
                token: $('#unique_token_restore').val(),
                new_password: restore_new_password,
            }, {
                start: function () {
                    $('#restore_submit_button').html('<span class="upload upload_white"><span></span><span></span><span></span></span>');
                },
                success: function (result, msg) {
                    nav.go('', '/news');
                },
                fail: function (result, error_msg) {
                    $('#restore_submit_button').html('Изменить пароль и авторизоваться');
                    $('#form_restore_error').show().html('<div class="error_msg">' + error_msg + '</div>');
                }
            });
        }
    },
    mail: function () {
        var value = isValueExst($('#login'));

        if (!value) {
            $('.error.login').show().html('Введите логин или e-mail.');

            $('#login').focus();
            return false;
        } else {
            ajaxRequest('/restore', 'POST', {
                login: value,
            }, {
                start: function () {
                    $('#restore_submit_button').html('<span class="upload upload_white"><span></span><span></span><span></span></span>');
                },
                success: function (result, msg) {
                   // nav.go('', '/news');
                },
                fail: function (result, error_msg) {
                    $('#restore_submit_button').html('Отправить');
                    $('#form_mail_error').show().html('<div class="error_msg">' + error_msg + '</div>');
                }
            });
        }
    }
};


var Wall = {
    postChanged: function () {
        var text = $('.submit_post_inited').text();

        if (text.length) {
            $('.profile_post_field_wrap .placeholder').hide();
        } else {
            $('.profile_post_field_wrap .placeholder').show();
        }
    },
    showEditPost: function () {
        $('.profile_post_wrap .suggest').hide();
        $('.profile_submit_post_box').addClass('shown');

        $('html, body').click(function (e) {
            var post_box = $('.profile_submit_post_box');

            if (!post_box.is(e.target) && post_box.has(e.target).length === 0 && $('.submit_post_inited').text().length == 0) {
                $('.profile_post_wrap .suggest').show();
                $('.profile_submit_post_box').removeClass('shown');
            }
        });

        $('*[contenteditable]').on('paste', function (evnt) {
            evnt.preventDefault();
            var plain_text = (evnt.originalEvent || evnt).clipboardData.getData('text/plain');
            if (typeof plain_text !== undefined) {
                document.execCommand('insertText', false, plain_text);
            }
        });
    },
    hideSuggest: function () {
        ajaxRequest('/wall/suggest', 'POST', {
            type: 1,
        }, {
            start: function () {

            },
            success: function (result, msg) {
                $('.profile_post_wrap .suggest').remove();
            },
            fail: function (result, error_msg) {
            }
        });
    },
    submit: function () {
        var text = $('.submit_post_inited');

        if (text.text().length) {
            ajaxRequest('/wall/add', 'POST', {
                message: text.html(),
            }, {
                start: function () {
                    $('#add_wall').html('<span class="upload upload_white"><span></span><span></span><span></span></span>');
                },
                finish: function () {
                    $('#add_wall').html('Опубликовать');
                },
                success: function (result, msg) {
                    $('.submit_post_inited').html('');
                    $('.profile_submit_post_box').removeClass('shown');
                    $('.profile_post_field_wrap .placeholder').show();
                    text.blur();

                    if ($('.no_posts_wrap').length > 0) {
                        $('.no_posts_wrap').hide();
                        $('.profile_content_right').append('<div class="feed_list"></div>')
                    }

                    $('.feed_list').prepend(result.html);
                },
                fail: function (result, error_msg) {

                }
            });
        } else {
            text.focus();
        }
    },
    actionsMenuShow: function (e) {
        $(e).addClass('shown');
    },
    actionsMenuHide: function (e) {
        var icon = $(e);
        var actions_menu = icon.find('.actions_menu');

        setTimeout(function () {
            if (!actions_menu.is(':hover')) {
                $(e).removeClass('shown');
            }
        }, 350);
    },
    actionsMenuDelete: function (id) {
        ajaxRequest('/wall/delete', 'POST', {
            id: id,
        }, {
            success: function (result, msg) {
                $('#feed_' + id).fadeOut(300, function () {
                    $(this).remove();
                });
            },
            fail: function (result, error_msg) {

            }
        });
    }
};

var Likes = {
    toggle: function (wall, event, wall_id) {
        var like = $(wall);
        var like_button_count = like.find('.like_button_count');
        var like_count = parseInt(like_button_count.text());
        var type = '';

        if (like.hasClass('empty')) {
            type = 'like';
        }

        if (!like.hasClass('empty') && like.hasClass('active')) {
            type = 'unLike';
        }

        if (!like.hasClass('empty') && !like.hasClass('active')) {
            type = 'like';
        }

        ajaxRequest('/wall/like', 'POST', {
            type: type,
            wall_id: wall_id,
        }, {
            start: function () {
                if (type == 'like') {
                    like.addClass('active');

                    if (isNaN(like_count)) {
                        like.removeClass('empty');
                        like_button_count.html(1);
                    } else {
                        like_button_count.html(like_count + 1);
                    }
                } else if (type == 'unLike') {
                    like.removeClass('active');

                    if (like_count === 1) {
                        like_button_count.html('');
                        like.addClass('empty');
                    } else {
                        like_button_count.html(like_count - 1);
                    }
                }
            },
            success: function (result, msg) {
            },
            fail: function (result, error_msg) {

            }
        });
    }
};
