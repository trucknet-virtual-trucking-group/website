if (!window._ua) {
    var _ua = navigator.userAgent.toLowerCase();
}

var browser = {
    version: (_ua.match(/.+(?:me|ox|on|rv|it|era|ie)[\/: ]([\d.]+)/) || [0, '0'])[1],
    opera: /opera/i.test(_ua),
    msie: (/msie/i.test(_ua) && !/opera/i.test(_ua)),
    msie8: (/msie 8/i.test(_ua) && !/opera/i.test(_ua)),
    msie9: (/msie 9/i.test(_ua) && !/opera/i.test(_ua)),
    mozilla: /firefox/i.test(_ua),
    chrome: /chrome/i.test(_ua),
    safari: (!(/chrome/i.test(_ua)) && /webkit|safari|khtml/i.test(_ua)),
    iphone: /iphone/i.test(_ua),
    ipod: /ipod/i.test(_ua),
    iphone4: /iphone.*OS 4/i.test(_ua),
    ipod4: /ipod.*OS 4/i.test(_ua),
    ipad: /ipad/i.test(_ua),
    android: /android/i.test(_ua),
    bada: /bada/i.test(_ua),
    mobile: /iphone|ipod|ipad|opera mini|opera mobi|iemobile|android/i.test(_ua),
    msie_mobile: /iemobile/i.test(_ua),
    safari_mobile: /iphone|ipod|ipad/i.test(_ua),
    opera_mobile: /opera mini|opera mobi/i.test(_ua),
    opera_mini: /opera mini/i.test(_ua),
    mac: /mac/i.test(_ua)
};

function Tpl() {
    this.vars = [];
}

extend(Tpl.prototype, {
    templates: {},
    quoted: function (s) {
        return "'" + jsescape(s) + "'";
    },
    q: function (s) {
        return this.quoted(s);
    },
    assign: function (name, value) {
        if (isObject(name)) {
            for (var i in name)
                this.vars[i] = name[i];
            return;
        }
        this.vars[name] = value;
        return this;
    },
    fetch: function (template, vars) {
        var template = Tpl.prototype.templates[template];
        var fun = new Function("obj", "var p=[],print=function(){p.push.apply(p,arguments);};" +
            "with(obj){p.push('" + template
                .replace(/[\r\t\n]/g, " ")
                .split("<?").join("\t")
                .replace(/((^|\?>)[^\t]*)'/g, "$1\r")
                .replace(/\t=(.*?)\?>/g, "',$1,'")
                .split("\t").join("');")
                .split("?>").join("p.push('")
                .split("\r").join("\\'")
            + "');}return p.join('');");

        if (vars)
            this.assign(vars);

        return fun(this.vars);
    }
});

extend(Tpl.prototype.templates, {
    'Notifications.notification': '<div class="notification clearfix"><div class="close" onclick="Notifications.hide(<?= id ?>)"> \
							            <div class="w"> \
							                <div></div> \
							                <div></div> \
							            </div> \
							        </div> \
							        <a href="#" class="pic fl_l"> \
							            <img src="<?= pic ?>" width="40"> \
							        </a> \
							        <div class="head"><a href="#"><?= title ?></a></div> \
							        <div class="title <?= whitespace_class ?>"><a href="#"><?= content ?></a></div></div>'
});

function extend(dest, source) {
    for (var i in source) {
        dest[i] = source[i];
    }
}

function hide(el) {
    el = ge(el);
    setStyle(el, 'display', 'none');
}

function ge(el) {
    return (typeof el == 'string' || typeof el == 'number') ? document.getElementById(el) : el;
}

function isArray(obj) {
    return Object.prototype.toString.call(obj) === '[object Array]';
}

function isObject(obj) {
    return Object.prototype.toString.call(obj) === '[object Object]';
}

function remove(el) {
    el = ge(el);
    if (el && el.parentNode) el.parentNode.removeChild(el);
    return el;
}

function addClass(obj, name) {
    if ((obj = ge(obj)) && !hasClass(obj, name))
        obj.className = (obj.className ? obj.className + ' ' : '') + name;
}

function removeClass(obj, name) {
    if (obj = ge(obj))
        obj.className = trim((obj.className || '').replace((new RegExp('(\\s|^)' + name + '(\\s|$)')), ' '));
}

function basename(path, suffix) {
    var b = path.replace(/^.*[\/\\]/g, '');
    if (typeof (suffix) == 'string' && b.substr(b.length - suffix.length) == suffix) {
        b = b.substr(0, b.length - suffix.length);
    }
    return b;
}

function inArray(needle, haystack) {
    for (var i = 0, l = haystack.length; i < l; i++) {
        if (needle == haystack[i])
            return true;
    }
    return false;
}

function hasClass(obj, name) {
    obj = ge(obj);
    return obj && (new RegExp('(\\s|^)' + name + '(\\s|$)')).test(obj.className);
}

function trim(string) {
    return string.replace(/(^\s+)|(\s+$)/g, "");
}

function setStyle(elem, name, value) {
    if (isArray(elem)) {
        for (var i = 0, l = elem.length; i < l; i++) {
            setStyle(elem[i], name, value);
        }
        return;
    }

    if (isObject(name)) {
        for (var key in name) {
            setStyle(elem, key, name[key]);
        }
        return;
    }

    if (!elem)
        return;
    if (name.toLowerCase() == 'opacity' && browser.msie && parseFloat(browser.version) < 9) {
        typeof elem.filters.alpha != 'undefined' ? elem.filters.alpha.opacity = value * 100 : elem.style.filter = 'alpha(opacity=' + value * 100 + ');';
    }

    elem.style[name] = value;
}

function addEvent(elem, type, handler) {
    if (document.addEventListener)
        elem.addEventListener(type, handler, false);
    else
        elem.attachEvent("on" + type, function () {
            handler.call(elem)
        });
}

function iplaceholder(a) {
    var placeholder = $(a).attr('iplaceholder');
    var value = $(a).val();

    if (!value) {
        setTimeout(function () {
            $(a).val(placeholder);
        }, 100);

        $(a).addClass('placeholder');
    }

    $(a).focus(function () {
        $(a).removeClass('placeholder');

        if ($(this).val() == placeholder) $(this).val('');
    });

    $(a).blur(function () {
        if (!$(this).val()) {
            $(this).val(placeholder);
            $(a).addClass('placeholder');
        }
    });
}

function isValue(a) {
    return ($(a).attr('iplaceholder') == $(a).val()) ? 0 : 1;
}

function isValueExst(a) {
    return (a.attr('iplaceholder') == a.val()) ? '' : a.val();
}

var stManager = {
    callbacks: [],
    resources: [],
    load: function (href, type) {
        if (inArray(href, this.resources)) return;
        var file = this.parsePath(href), self = this;
        this.resources.push(href);
        switch (type) {
            case 'js':
                var script = create('script', {
                    src: href,
                    type: 'text/javascript'
                });
                document.getElementsByTagName('head')[0].appendChild(script);
                break;
            case 'css':
                var link = create('link', {
                    href: href,
                    rel: 'stylesheet',
                    type: 'text/css'
                });
                link.onload = function () {
                    self.done(file);
                };
                document.getElementsByTagName('head')[0].appendChild(link);
                break;
        }
    },
    init: function () {
        var scripts = document.getElementsByTagName('script');
        for (var i in scripts) {
            if (scripts[i].src) {
                this.resources.push(scripts[i].src);
            }
        }
        var links = document.getElementsByTagName('link');
        for (var i in links) {
            if (links[i].href && links[i].rel == 'stylesheet') {
                this.resources.push(links[i].href);
            }
        }
    },
    done: function (name) {
        this.callbacks[name] = isArray(this.callbacks[name]) ? this.callbacks[name] : [];
        for (var i = 0; i < this.callbacks[name].length; i++) {
            this.callbacks[name][i]();
        }
    },
    ral: function (name, callback) {
        this.callbacks[name] = isArray(this.callbacks[name]) ? this.callbacks[name] : [];
        this.callbacks[name].push(callback);
    },
    parsePath: function (path) {
        var match = path.match(/^(https?:\/\/([\w\.-]+))?(\/.*)$/);
        if (!match || !match[3]) return path;
        path = basename(match[3]);
        var pos = path.indexOf('?');
        if (pos != -1) {
            path = path.substring(0, pos);
        }
        return path;
    }
};

function create(tagName, attributes) {
    var el = document.createElement(tagName);
    if (isObject(attributes)) {
        for (var i in attributes) {
            el.setAttribute(i, attributes[i]);
            if (i.toLowerCase() == 'class') {
                el.className = attributes[i];
            } else if (i.toLowerCase() == 'style') {
                el.style.cssText = attributes[i];
            }
        }
    }
    return el;
}

function getScrollBarWidth() {
    var inner = document.createElement('p');

    inner.style.width = '100%';
    inner.style.height = '200px';

    var outer = document.createElement('div');

    outer.style.position = 'absolute';
    outer.style.top = '0px';
    outer.style.left = '0px';
    outer.style.visibility = 'hidden';
    outer.style.width = '200px';
    outer.style.height = '150px';
    outer.style.overflow = 'hidden';
    outer.appendChild(inner);

    document.body.appendChild(outer);
    var w1 = inner.offsetWidth;
    outer.style.overflow = 'scroll';
    var w2 = inner.offsetWidth;
    if (w1 == w2) w2 = outer.clientWidth;

    document.body.removeChild(outer);

    return (w1 - w2);
}

function isName(name) {
    return name.match(/^[а-яё]+$/i);
}

function isLogin(login) {
    return login.match(/^([@a-zA-Z0-9\-\.\_\*]){2,20}$/gi);
}

function isPassword(password) {
    return password.match(/^([@a-zA-Z0-9\-\.\_\*]){6,30}$/gi);
}

function isEmail(email) {
    return email.match(/^[a-zA-Z0-9_\.\-\_]+@([a-zA-Z0-9\-\_]+\.)+[a-zA-Z]{2,6}$/gi);
}

function pageUpScroll() {
    $('html, body').animate({scrollTop: $('body').offset().top}, 300);
}
