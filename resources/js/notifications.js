var Notifications = {
    counter: 0,
    shown: [],
    show: function (params) {
        this.counter++;

        var id = this.counter, height = 0, self = this, notifications_wrap = ge('notifications_wrap'),
            notification = create('div', {
                'class': 'notification_wrap',
                'id': 'notification_wrap' + id
            });

        notification.innerHTML = new Tpl().fetch('Notifications.notification', {
            'id': id,
            'pic': params.pic,
            'title': params.title,
            'content': params.content,
            'whitespace_class': (params.whitespace) ? '' : 'nowrap'
        });

        notifications_wrap.insertBefore(notification, notifications_wrap.firstChild);
        height = notification.offsetHeight;

        remove(notification);

        notifications_wrap.appendChild(notification);
        removeClass(notifications_wrap, 'anim');
        setStyle(notifications_wrap, 'bottom', -height + 'px');
        setStyle(notification, 'display', 'block');

        setTimeout(function () {
            setStyle(notification, 'opacity', 1);
            addClass(notifications_wrap, 'anim');
            setStyle(notifications_wrap, 'bottom', 0);
        }, 10);

        var del_timeout = setTimeout(function () {
            self.hide(id);
        }, 5000);

        addEvent(notification, 'mousemove', function () {
            clearTimeout(del_timeout);
        });

        addEvent(notification, 'mouseout', function () {
            del_timeout = setTimeout(function () {
                self.hide(id);
            }, 5000);
        });
    },
    hide: function (id) {
        remove(ge('notification_wrap' + id));
    }
}
