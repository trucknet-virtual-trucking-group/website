<?php

return [
    'token' => env('VK_API_TOKEN', 'No token'),
    'album_ids' => env('VK_API_ALBUM_IDS', 'No Albums Ids'),
    'group_ids' => env('VK_API_GROUP_IDS', 'No Groups Ids')
];
