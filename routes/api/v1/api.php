<?php

Route::group(['prefix' => 'v1'], function () {
    Route::get('/getCities', 'Cities\CitiesController@cities');
    Route::get('/getCountries', 'Countries\CountriesController@countries');
    Route::get('/captcha', 'Captcha\CaptchaController@create')->name('Captcha');

    Route::post('/auth', 'Auth\AuthController@auth')->name('api.auth');
});
