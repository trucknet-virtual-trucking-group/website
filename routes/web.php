<?php

# Главная страница
use Illuminate\Support\Facades\Storage;

Route::get('/', 'Main\MainController@index')->name('index');

# Регистрация
Route::post('/reg', 'Api\V1\Reg\RegController@reg')->name('api.reg');

# Авторизация
Route::post('/login', 'Auth\AuthController@login')->name('auth.login');

Route::get('auth/{social}', 'Auth\AuthController@redirectToProvider')
    ->where('social', 'vkontakte|Google|yandex|odnoklassniki|mailru')
    ->name('auth.social');
Route::get('auth/{social}/callback', 'Auth\AuthController@handleProviderCallback')
    ->where('social', 'vkontakte|Google|yandex|odnoklassniki|mailru')
    ->name('auth.social.callback');

# Восстановление доступа
Route::get('/restore', 'User\RestoreController@index')->name('restore');
Route::post('/restore', 'User\RestoreController@restore')->name('restore.token');
Route::post('/restore/newpass', 'User\RestoreController@newPass')->name('restore.newpass');

Route::group(['middleware' => ['auth']], function () {
    # Профиль пользователя
    Route::get('/id{id}', 'User\UserController@show')->name('user.profile');

    # Получение постов через ajax
    Route::get('/wall', 'User\Wall\WallController@ajaxPagination')->name('user.wall.ajax');

    # Добавление поста на стену.
    Route::post('/wall/add', 'User\Wall\WallController@create')->name('user.wall.create');

    # Удаление поста
    Route::post('/wall/delete', 'User\Wall\WallController@delete')->name('user.wall.delete');

    # Лайк на пост
    Route::post('/wall/like', 'User\Wall\WallController@like')->name('user.wall.like');

    # Скрытие предложения добавления поста.
    Route::post('/wall/suggest', 'User\Wall\WallSuggestController@hide')->name('user.wall.suggest.hide');

    # Сохранение логина и пароля.
    Route::post('/profile/completion', 'User\UserController@completion')->name('user.profile.completion');

    # Загрузка аватарки
    Route::post('/profile/upload/avatar', 'User\UserController@uploadAvatar')->name('user.profile.upload.avatar');

    # Загрузка обложки
    Route::post('/profile/upload/cover', 'User\UserController@uploadCover')->name('user.profile.upload.cover');

    # Удаление обложки
    Route::post('/profile/delete/cover', 'User\UserController@deleteCover')->name('user.profile.delete.cover');

    # Новости
    Route::get('/news', 'Pages\News\NewsController@index')->name('news');

    # Общий чат
    Route::get('/chat', 'Pages\Chat\ChatController@index')->name('chat');

    # Компании
    Route::get('/vtc', 'Pages\Vtc\VtcController@index')->name('vtc');

    # Пользователи
    Route::get('/users', 'Pages\Users\UsersController@index')->name('users');

    # Конкурсы
    Route::get('/contests', 'Pages\Contests\ContestsController@index')->name('contests');

    # Задания
    Route::get('/tasks', 'Pages\Tasks\TasksController@index')->name('tasks');

    # Файлы
    Route::get('/files', 'Pages\Files\FilesController@index')->name('files');

    # Контракты
    Route::get('/contracts', 'Pages\Contracts\ContractController@index')->name('contract');

    # Плагин
    Route::get('/plugin', 'Pages\Plugin\PluginController@index')->name('plugin');

    # Форум
    Route::get('/forum', 'Pages\Forum\ForumController@index')->name('forum');

    # Помощь
    Route::get('/support', 'Pages\Support\SupportController@index')->name('support');

    # Баланс
    Route::get('/balance', 'Pages\Users\Balance\BalanceController@index')->name('balance');

    # Выход
    Route::get('/logout', 'User\UserController@logout')->name('logout');
});

/**
 * Admin Routes
 */
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'group:administration']], function () {
    Route::group(['prefix' => 'dashboard'], static function () {
        Route::get('/', 'Admin\DashboardController@index')->name('admin.dashboard');
        #Route::get('all_users', 'Admin\DashboardController@all_users')->name('admin.dashboard.all_users');
        #Route::get('all_convoys', 'Admin\DashboardController@all_convoys')->name('admin.dashboard.all_convoys');
        #Route::get('all_comments', 'Admin\DashboardController@all_comments')->name('admin.dashboard.all_comments');
    });
});
