<?php

namespace App\Providers;

use App\Repositories\ETS2\TruckersMP\EloquentServersRepository;
use App\Repositories\ETS2\TruckersMP\ServersRepository;
use App\Repositories\ETS2\Trucks\EloquentTrucksRepository;
use App\Repositories\ETS2\Trucks\TrucksRepository;
use App\Repositories\Images\EloquentImageRepository;
use App\Repositories\Images\ImageRepository;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Repositories\User\UserRepository;
use App\Repositories\User\EloquentUserRepository;
use App\Helpers\DateHelper;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        if(config('app.env') === 'production') {
            \URL::forceScheme('https');
        }

        Schema::defaultStringLength(191);
        $this->app->bind(UserRepository::class, EloquentUserRepository::class);
        $this->app->bind(ServersRepository::class, EloquentServersRepository::class);
        $this->app->bind(TrucksRepository::class, EloquentTrucksRepository::class);
        $this->app->bind(ImageRepository::class, EloquentImageRepository::class);

        //Register the Starting Tag
        Blade::directive('modifiedScript', function () {
            return "<?php echo (new App\Helpers\Parser\Scripts)->modifiedScript(\"";
        });
        //Register the Ending Tag
        Blade::directive('endmodifiedScript', function () {
            return "\"); ?>";
        });
    }
}
