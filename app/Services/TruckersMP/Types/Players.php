<?php

namespace App\Services\TruckersMP\Types;

use Exception;

class Players
{
    public $players;

    /**
     * Players constructor.
     * @param array $response
     * @throws Exception
     */
    public function __construct(array $response)
    {
        $this->players = $response['response'];
    }
}
