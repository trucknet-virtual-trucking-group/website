<?php

namespace App\Services\TruckersMP\API;

use App\Services\TruckersMP\Exceptions\APIErrorException;
use App\Services\TruckersMP\Exceptions\PlayerNotFoundException;
use App\Services\TruckersMP\Types\Bans;
use App\Services\TruckersMP\Types\GameTime;
use App\Services\TruckersMP\Types\Player;
use App\Services\TruckersMP\Types\Players;
use App\Services\TruckersMP\Types\Servers;
use App\Services\TruckersMP\Types\Version;
use Psr\Http\Client\ClientExceptionInterface;

class APIClient
{
    public const API_ENDPOINT = 'api.truckersmp.com';
    public const API_VERSION = 'v2';

    /**
     * @var Request
     */
    private $request;

    /**
     * APIClient constructor.
     *
     * @param array $config
     * @param bool $secure
     */

    public function __construct(array $config = [], $secure = true)
    {
        $scheme = $secure ? 'https' : 'http';
        $url = $scheme . '://' . self::API_ENDPOINT . '/' . self::API_VERSION . '/';

        $this->request = new Request($url, $config);
    }

    /**
     * Fetch player information.
     *
     * @param int $id
     *
     * @return Player
     * @throws PlayerNotFoundException
     * @throws ClientExceptionInterface
     */
    public function player($id): Player
    {
        $result = $this->request->execute('player/' . $id);

        return new Player($result);
    }

    /**
     * @param $id
     *
     * @return Bans
     * @throws APIErrorException
     * @throws ClientExceptionInterface
     * @throws PlayerNotFoundException
     */
    public function bans($id): Bans
    {
        $result = $this->request->execute('bans/' . $id);

        return new Bans($result);
    }

    /**
     * @return Servers
     * @throws APIErrorException
     * @throws ClientExceptionInterface
     */
    public function servers(): Servers
    {
        $result = $this->request->execute('servers');

        return new Servers($result);
    }

    /**
     * @return GameTime
     * @throws ClientExceptionInterface
     * @throws \Exception
     */
    public function gameTime(): GameTime
    {
        $result = $this->request->execute('game_time');

        return new GameTime($result);
    }

    /**
     * @return Version
     * @throws ClientExceptionInterface
     * @throws \Exception
     */
    public function version(): Version
    {
        $result = $this->request->execute('version');

        return new Version($result);
    }

    /**
     * @return Players
     * @throws ClientExceptionInterface
     * @throws \Exception
     */
    public function players(): Players
    {
        $result = $this->request->execute('players');

        return new Players($result);
    }
}
