<?php

namespace App\Services\TruckersMP\API;

use GuzzleHttp\Client as GuzzleClient;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use Http\Message\MessageFactory\GuzzleMessageFactory;
use Psr\Http\Client\ClientExceptionInterface;

class Request
{
    /**
     * @var GuzzleMessageFactory
     */
    private $message;

    /**
     * @var string
     */
    private $apiEndpoint;

    /**
     * @var GuzzleAdapter
     */
    private $adapter;

    /**
     * Request constructor.
     *
     * @param $apiEndpoint
     * @param array $config
     */
    public function __construct($apiEndpoint, $config)
    {
        $this->message = new GuzzleMessageFactory();

        $this->apiEndpoint = $apiEndpoint;
        $this->adapter = new GuzzleAdapter(new GuzzleClient($config));
    }

    /**
     * @param string $uri URI of API method
     * @return array
     * @throws ClientExceptionInterface
     */
    public function execute($uri): array
    {
        $request = $this->message->createRequest('GET', $this->apiEndpoint . $uri);
        $result = $this->adapter->sendRequest($request);

        return json_decode((string)$result->getBody(), true, 512, JSON_BIGINT_AS_STRING);
    }
}
