<?php

namespace App\Services\User;

use App\Models\User\User;
use App\Services\BaseService;
use App\Repositories\User\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;
use App\Services\TruckersMP\API\APIClient as TMPClient;
use App\Services\TruckersMP\Types\Player;
use Throwable;
use VK\Client\VKApiClient;
use VK\Exceptions\VKApiException;
use VK\Exceptions\VKClientException;

class UserService extends BaseService
{
    /** @var UserRepository */
    protected $userRepository;

    /** @var TMPClient */
    protected $truckersmpClient;

    /** @var VKApiClient */
    protected $vk;

    /**
     * UserService constructor.
     *
     * @param UserRepository $userRepository
     * @param TMPClient $truckersmpClient
     * @param VKApiClient $vk
     */
    public function __construct(
        UserRepository $userRepository,
        TMPClient $truckersmpClient,
        VKApiClient $vk
    )
    {
        $this->userRepository = $userRepository;
        $this->truckersmpClient = $truckersmpClient;
        $this->vk = $vk;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function login(Request $request)
    {
        $login = $request->get('login');
        $password = $request->get('password');

        $user = $this->userRepository->findByLogin($login);

        if (!$user) {
            return [
                'response' => 0,
                'error_msg' => 'Пользователь не найден.'
            ];
        }

        if (!Hash::check($password, $user->password)) {
            return [
                'response' => 0,
                'error_msg' => 'Неверный пароль'
            ];
        }

        auth()->login($user, true);

        return ['response' => 1];
    }

    /**
     * @param User $user
     * @return bool
     */
    public function attachTruckersMp(User $user)
    {
        $player = $this->truckersMp_check($user->steam_id);

        if ($player) {
            $user_data = [
                'mp_id' => $player->id,
                'name' => $player->name,
                'avatar' => $player->avatar,
                'smallAvatar' => $player->smallAvatar,
                'groupName' => $player->groupName,
                'groupID' => $player->groupID,
                'banned' => $player->banned,
                'bannedUntil' => $player->bannedUntil,
                'displayBans' => $player->displayBans,
                'inGameAdmin' => $player->inGameAdmin,
                'vtc' => $player->vtc ? json_encode($player->vtc) : [],
            ];

            $user->userTruckersMp()->create($user_data);
        }

        return true;
    }

    /**
     * @param $steam_id
     * @return Player
     */
    private function truckersMp_check($steam_id)
    {
        /** @var Player $tmp */
        /** @var TMPClient $client */
        try {
            return $this->truckersmpClient->player($steam_id);
        } catch (Throwable $e) {}

        return null;
    }

    /**
     * @param string $email
     * @return bool
     */
    public function findByEmailAndAuth(string $email)
    {
        $find_user = $this->userRepository->findByEmail($email);

        if ($find_user) {
            auth()->login($find_user, true);
            return true;
        }

        return false;
    }

    /**
     * @param \Laravel\Socialite\Contracts\User $user
     * @return User|null
     * @throws VKApiException
     * @throws VKClientException
     */
    public function createByVk(\Laravel\Socialite\Contracts\User $user)
    {
        $vk_fields = 'sex,bdate,city,country,has_photo,photo_max_orig';
        $vk_user = $this->vk->users()->get(config('vkcom.token'), ['user_ids' => $user->getId(), 'fields' => $vk_fields]);

        $user_data = [
            'firstName' => $vk_user[0]['first_name'],
            'lastName' => $vk_user[0]['last_name'],
            'country' => $vk_user[0]['country']['title'],
            'city' => $vk_user[0]['city']['title'],
            'gender' => $vk_user[0]['sex'],
            'dateOfBirth' => Carbon::parse($vk_user[0]['bdate'])->toDate(),
        ];

        $email = $user->getEmail();

        if ($email) {
            $user_data['email'] = $email;
        }

        $new_user = $this->userRepository->create($user_data);

        auth()->login($new_user, true);

        if ($vk_user[0]['has_photo']) {
            $photo_big = Image::make($vk_user[0]['photo_max_orig'])->stream('jpg', 100);
            $photo_big_name = Str::random(40) . '.jpg';

            Storage::disk('public')->put('images/users/' . $new_user->id . '/avatars/' . $photo_big_name, $photo_big);

            $create_image = $new_user->userImages()->create([
                'path' => 'images/users/' . $new_user->id . '/avatars/' . $photo_big_name,
            ]);

            if ($create_image) {
                $this->userRepository->update($new_user->id, ['avatar_id' => $create_image->id]);
            }

            return $new_user;
        }

        return null;
    }

    /**
     * @param \Laravel\Socialite\Contracts\User $user
     * @return User|null
     */
    public function createByOk(\Laravel\Socialite\Contracts\User $user)
    {
        $user_data = [
            'firstName' => $user->user['first_name'],
            'lastName' => $user->user['last_name'],
            'gender' => $user->user['gender'] === 'male' ? 2 : 1,
            'dateOfBirth' => Carbon::parse($user->user['birthday'])->toDate(),
            'email' => $user->getEmail(),
        ];

        if ($user->getEmail()) {
            $user_data['email'] = $user->getEmail();
        }

        $new_user = $this->userRepository->create($user_data);

        auth()->login($new_user, true);

        if ($user->getAvatar()) {
            $photo_big = Image::make($user->getAvatar())->stream('jpg', 100);
            $photo_big_name = Str::random(40) . '.jpg';

            Storage::disk('public')->put('images/users/' . $new_user->id . '/avatars/' . $photo_big_name, $photo_big);

            $create_image = $new_user->userImages()->create([
                'path' => 'images/users/' . $new_user->id . '/avatars/' . $photo_big_name,
            ]);

            if ($create_image) {
                $this->userRepository->update($new_user->id, ['avatar_id' => $create_image->id]);
            }

            return $new_user;
        }

        return null;
    }

    /**
     * @param \Laravel\Socialite\Contracts\User $user
     * @return User|null
     */
    public function createByYandex(\Laravel\Socialite\Contracts\User $user)
    {
        $user_data = [
            'firstName' => $user->user['first_name'],
            'lastName' => $user->user['last_name'],
            'gender' => $user->user['sex'] === 'male' ? 2 : 1,
            'dateOfBirth' => Carbon::parse($user->user['birthday'])->toDate(),
            'email' => $user->getEmail(),
        ];

        $new_user = $this->userRepository->create($user_data);

        auth()->login($new_user, true);

        if ($user->getAvatar()) {
            $photo_big = Image::make($user->getAvatar())->stream('jpg', 100);
            $photo_big_name = Str::random(40) . '.jpg';

            Storage::disk('public')->put('images/users/' . $new_user->id . '/avatars/' . $photo_big_name, $photo_big);

            $create_image = $new_user->userImages()->create([
                'path' => 'images/users/' . $new_user->id . '/avatars/' . $photo_big_name,
            ]);

            if ($create_image) {
                $this->userRepository->update($new_user->id, ['avatar_id' => $create_image->id]);
            }

            return $new_user;
        }

        return null;
    }

    /**
     * @param \Laravel\Socialite\Contracts\User $user
     * @return User|null
     */
    public function createByGoogle(\Laravel\Socialite\Contracts\User $user)
    {
        $user_data = [
            'firstName' => $user->user['given_name'],
            'lastName' => $user->user['family_name'],
            'email' => $user->getEmail(),
        ];

        $new_user = $this->userRepository->create($user_data);

        auth()->login($new_user, true);

        if ($user->getAvatar()) {
            $photo_big = Image::make($user->getAvatar())->stream('jpg', 100);
            $photo_big_name = Str::random(40) . '.jpg';

            Storage::disk('public')->put('images/users/' . $new_user->id . '/avatars/' . $photo_big_name, $photo_big);

            $create_image = $new_user->userImages()->create([
                'path' => 'images/users/' . $new_user->id . '/avatars/' . $photo_big_name,
            ]);

            if ($create_image) {
                $this->userRepository->update($new_user->id, ['avatar_id' => $create_image->id]);
            }

            return $new_user;
        }

        return null;
    }
}
