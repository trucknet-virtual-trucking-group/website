<?php

namespace App\Services\ETS2\TruckersMP\Servers;

use App\Models\TruckersMP\Servers;
use App\Services\BaseService;
use App\Repositories\ETS2\TruckersMP\ServersRepository;
use Illuminate\Database\Eloquent\Collection;

class ServersService extends BaseService
{
    /** @var ServersRepository */
    protected $serversRepository;

    /**
     * ServersService constructor.
     *
     * @param ServersRepository $serversRepository
     */
    public function __construct(ServersRepository $serversRepository)
    {
        $this->serversRepository = $serversRepository;
    }

    /**
     * @param array $data
     * @return Servers
     */
    public function create(array $data): Servers
    {
        return $this->serversRepository->create($data);
    }
}
