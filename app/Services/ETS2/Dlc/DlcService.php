<?php

namespace App\Services\ETS2\Dlc;

use App\Services\BaseService;
use App\Repositories\ETS2\Dlc\DlcRepository;

class DlcService extends BaseService
{
    /** @var DlcRepository */
    protected $dlcRepository;

    /**
     * DlcService constructor.
     * @param DlcRepository $dlcRepository
     */
    public function __construct(DlcRepository $dlcRepository)
    {
        $this->dlcRepository = $dlcRepository;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) {
        return $this->dlcRepository->create($data);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function findByName(string $name) {
        return $this->dlcRepository->findByName($name);
    }
}
