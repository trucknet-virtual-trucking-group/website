<?php

namespace App\Services\ETS2\Trucks;

use App\Models\ETS2\Trucks;
use App\Services\BaseService;
use App\Repositories\ETS2\Trucks\TrucksRepository;
use Illuminate\Database\Eloquent\Collection;

class TrucksService extends BaseService
{
    /** @var TrucksRepository */
    protected $trucksRepository;

    /**
     * TrucksService constructor.
     *
     * @param TrucksRepository $trucksRepository
     */
    public function __construct(TrucksRepository $trucksRepository)
    {
        $this->trucksRepository = $trucksRepository;
    }

    /**
     * @param array $data
     * @return Trucks
     */
    public function create(array $data): Trucks
    {
        return $this->trucksRepository->create($data);
    }
}
