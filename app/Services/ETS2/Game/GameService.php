<?php

namespace App\Services\ETS2\Game;

use App\Services\BaseService;
use App\Repositories\ETS2\Game\GameRepository;

class GameService extends BaseService
{
    /** @var GameRepository */
    protected $game;

    /**
     * GameService constructor.
     *
     * @param GameRepository $game
     */
    public function __construct(GameRepository $game)
    {
        $this->game = $game;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) {
        return $this->game->create($data);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function findByName(string $name) {
        return $this->game->findByName($name);
    }
}
