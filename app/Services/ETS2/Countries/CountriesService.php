<?php

namespace App\Services\ETS2\Countries;

use App\Services\BaseService;
use App\Repositories\ETS2\Countries\CountriesRepository;

class CountriesService extends BaseService
{
    /** @var CountriesRepository */
    protected $countriesRepository;

    /**
     * CountriesService constructor.
     *
     * @param CountriesRepository $countriesRepository
     */
    public function __construct(CountriesRepository $countriesRepository)
    {
        $this->countriesRepository = $countriesRepository;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data) {
        return $this->countriesRepository->create($data);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function findByName(string $name) {
        return $this->countriesRepository->findByName($name);
    }
}
