<?php

namespace App\Services\ETS2\Cities;

use App\Models\ETS2\Cities;
use App\Repositories\ETS2\Cities\CitiesRepository;
use App\Services\BaseService;

class CitiesService extends BaseService
{
    /** @var CitiesRepository */
    protected $citiesRepository;

    /**
     * CitiesService constructor.
     *
     * @param CitiesRepository $citiesRepository
     */
    public function __construct(CitiesRepository $citiesRepository)
    {
        $this->citiesRepository = $citiesRepository;
    }

    /**
     * @param array $data
     * @return Cities
     */
    public function create(array $data): Cities
    {
        return $this->citiesRepository->create($data);
    }

    /**
     * @param string $name
     * @return Cities
     */
    public function findByName(string $name): Cities
    {
        return $this->citiesRepository->findByName($name);
    }
}
