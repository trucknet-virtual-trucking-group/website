<?php

namespace App\Services\VkApi;

use PHPUnit\Util\Json;
use VK\Client\VKApiClient;
use VK\Exceptions\VKApiException;
use VK\Exceptions\VKClientException;

class VKApiService
{
    /** @var VKApiClient */
    protected $vkApiClient;

    /** @var string */
    protected $token;

    /** @var string */
    const code = 'RU,UA,BY,BE,EE,AI,VI,AS,AM,AF';

    /**
     * VKApiService constructor.
     * @param VKApiClient $vkApiClient
     */
    public function __construct(VKApiClient $vkApiClient)
    {
        $this->vkApiClient = $vkApiClient;
        $this->token = config('vkcom.token');
    }

    /**
     * @param bool $need_all
     * @return mixed
     * @throws VKApiException
     * @throws VKClientException
     */
    public function getCountries(bool $need_all = true)
    {
        return $this->vkApiClient->database()->getCountries($this->token, ['need_all' => $need_all, 'code' => self::code]);
    }

    /**
     * @param int $country_id
     * @param string $q
     * @return mixed
     * @throws VKApiException
     * @throws VKClientException
     */
    public function getCities(int $country_id, string $q = '')
    {
        return $this->vkApiClient->database()->getCities($this->token, ['country_id' => $country_id, 'q' => $q]);
    }

    /**
     * @param string $url
     * @return mixed
     * @throws VKApiException
     * @throws VKClientException
     */
    public function getUser(string $url)
    {
        $newUrl = str_replace(['http://vk.com/', 'https://vk.com/'], '', $url);
        return $this->vkApiClient->users()->get($this->token, ['user_ids' => $newUrl]);
    }
}
