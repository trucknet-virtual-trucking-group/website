<?php

namespace App\Services\History;

use App\Models\History\History;
use App\Services\BaseService;
use App\Repositories\History\HistoryRepository;

class HistoryService extends BaseService
{
    /** @var HistoryRepository */
    protected $historyRepository;

    /**
     * HistoryService constructor.
     *
     * @param HistoryRepository $historyRepository
     */
    public function __construct(HistoryRepository $historyRepository)
    {
        $this->historyRepository = $historyRepository;
    }

    /**
     * @param array $data
     * @return History
     */
    public function create(array $data) {
        return $this->historyRepository->create($data);
    }
}
