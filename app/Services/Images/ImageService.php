<?php

namespace App\Services\Images;

use App\Repositories\User\UserRepository;
use Auth;
use Illuminate\Database\Eloquent\Model;
use VK\Client\VKApiClient;
use VK\Exceptions\Api\VKApiParamAlbumIdException;
use VK\Exceptions\Api\VKApiParamHashException;
use VK\Exceptions\Api\VKApiParamServerException;
use VK\Exceptions\VKApiException;
use VK\Exceptions\VKClientException;

class ImageService
{
    /**
     * @var VKApiClient
     */
    private $vk;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $albumIds;

    /**
     * @var string
     */
    private $groupIds;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * ImageService constructor.
     *
     * @param VKApiClient $vKApiClient
     * @param UserRepository $userRepository
     */
    public function __construct(VKApiClient $vKApiClient, UserRepository $userRepository)
    {
        $this->vk = $vKApiClient;
        $this->userRepository = $userRepository;
        $this->token = config('vkcom.token');
        $this->albumIds = config('vkcom.album_ids');
        $this->groupIds = config('vkcom.group_ids');
    }

    /**
     * @return int
     */
    private function getAlbumId()
    {
        $album_ids = explode(',', $this->albumIds);
        $rand = array_rand($album_ids, 1);

        return (int)$album_ids[$rand];
    }

    /**
     * @return int
     */
    private function getGroupId()
    {
        return (int)$this->groupIds;
    }

    /**
     * @param int $user_id
     * @param string $path
     * @return Model|bool
     */
    public function save(int $user_id, string $path)
    {
        $user = $this->userRepository->getUserById($user_id);

        $album_id = $this->getAlbumId();
        $group_id = $this->getGroupId();

        try {
            $address = $this->vk->photos()->getUploadServer($this->token, [
                'album_id' => $album_id,
                'group_id' => $group_id,
            ]);

            $photo = $this->vk->getRequest()->upload($address['upload_url'], 'photo', $path);

            $response_save_photo = $this->vk->photos()->save($this->token, [
                'album_id' => $album_id,
                'group_id' => $group_id,
                'server' => $photo['server'],
                'photos_list' => $photo['photos_list'],
                'hash' => $photo['hash'],
            ]);

            $availableSize = ['s', 'm', 'x', 'o', 'p', 'q', 'r', 'y', 'z', 'w'];
            $imageCreateData = [];

            if (count($response_save_photo[0]['sizes'])) {
                foreach ($response_save_photo[0]['sizes'] as $size) {
                    if (in_array($size['type'], $availableSize)) {
                        $imageCreateData[$size['type']] = $size['url'];
                    }
                }
            }

        } catch (VKApiException $e) {
            return false;
        } catch (VKClientException $e) {
            return false;
        }

        return $user->userImages()->create($imageCreateData);
    }

    /**
     * @param int $user_id
     * @param string $path
     * @return bool|Model
     */
    public function updateUserAvatar(int $user_id, string $path)
    {
        $image = $this->save($user_id, $path);

        if ($image instanceof Model) {
            if ($this->userRepository->update($user_id, ['avatar_id' => $image->id])) {
                return $image;
            }

            return false;
        }

        return false;
    }

    /**
     * @param int $user_id
     * @param string $path
     * @return bool|Model
     */
    public function uploadUserCover(int $user_id, string $path)
    {
        $image = $this->save($user_id, $path);

        if ($image instanceof Model) {
            if ($this->userRepository->update($user_id, ['cover_id' => $image->id])) {
                return $image;
            }

            return false;
        }

        return false;
    }
}
