<?php

namespace App\Repositories\History;

use App\Models\History\History;
use App\Repositories\BaseRepository;

interface HistoryRepository extends BaseRepository
{
    /**
     * @param array $data
     * @return History
     */
    public function create(array $data);
}
