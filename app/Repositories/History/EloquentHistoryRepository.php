<?php

namespace App\Repositories\History;

use App\Models\History\History;
use Illuminate\Database\Eloquent\Model;

class EloquentHistoryRepository implements HistoryRepository
{
    /** @var History */
    protected $history;

    /**
     * EloquentHistoryRepository constructor.
     *
     * @param History $history
     */
    public function __construct(History $history)
    {
        $this->history = $history;
    }

    /**
     * @param array $data
     * @return History|Model
     */
    public function create(array $data) {
        return $this->history->create($data);
    }
}
