<?php

namespace App\Repositories\User;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use HttpOz\Roles\Models\Role;

class EloquentUserRepository implements UserRepository
{
    /** @var User */
    protected $user;

    /** @var Role */
    protected $role;

    /**
     * EloquentUserRepository constructor.
     *
     * @param User $user
     * @param Role $role
     */
    public function __construct(User $user, Role $role)
    {
        $this->user = $user;
        $this->role = $role;
    }

    /**
     * @param array $data
     * @return Model
     */
    public function create(array $data)
    {
        $created_user = $this->user->create($data);
        $created_user->attachRole($this->role->whereSlug('user')->first());

        return $created_user;
    }

    /**
     * @param int $id
     * @return User|User[]|Collection|Model|null
     */
    public function getUserById(int $id)
    {
        return $this->user->find($id);
    }

    /**
     * @param int $id
     * @param array $data
     * @return bool
     */
    public function update(int $id, array $data)
    {
        return $this->user->where('id', $id)->update($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return int
     */
    public function updateSettings(int $id, array $data)
    {
        return $this->user->find($id)->userSettings()->update($data);
    }

    /**
     * @param int $steamId
     * @return User|Builder|object
     */
    public function findBySteam(int $steamId)
    {
        return $this->user->where('steam_id', '=', $steamId)->first();
    }

    /**
     * @param int $steamId
     * @return bool
     */
    public function existsBySteam(int $steamId)
    {
        $find = $this->user->where('steam_id', '=', $steamId)->count();

        return (bool) $find;
    }

    /**
     * @param string $email
     * @return Builder|User|object|null
     */
    public function findByEmail(string $email)
    {
        return $this->user->where('email', $email)->first();
    }

    /**
     * @param string $login
     * @return User|Builder|Model|object|null
     */
    public function findByLogin(string $login)
    {
        return $this->user->where('login', $login)->first();
    }

    /**
     * @param int $id
     * @return User|Builder|Model|object|null
     */
    public function findByVkId(int $id)
    {
        return $this->user->where('vk_id', $id)->first();
    }
}
