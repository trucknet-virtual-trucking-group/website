<?php

namespace App\Repositories\User;

use App\Repositories\BaseRepository;
use App\Models\User\User;

interface UserRepository extends BaseRepository
{
    /**
     * @param array $data
     * @return User
     */
    public function create(array $data);

    /**
     * @param int $id
     * @return User
     */
    public function getUserById(int $id);

    /**
     * @param int $id
     * @param array $data
     * @return bool
     */
    public function update(int $id, array $data);

    /**
     * @param int $id
     * @param array $data
     * @return int
     */
    public function updateSettings(int $id, array $data);

    /**
     * @param int $steamId
     * @return User
     */
    public function findBySteam(int $steamId);

    /**
     * @param int $steamId
     * @return bool
     */
    public function existsBySteam(int $steamId);

    /**
     * @param string $email
     * @return User|null
     */
    public function findByEmail(string $email);

    /**
     * @param string $login
     * @return User|null
     */
    public function findByLogin(string $login);

    /**
     * @param int $id
     * @return User|null
     */
    public function findByVkId(int $id);
}
