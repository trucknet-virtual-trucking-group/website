<?php

namespace App\Repositories\ETS2\Countries;

use App\Models\ETS2\Countries;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class EloquentCountriesRepository implements CountriesRepository
{
    /** @var Countries */
    protected $countries;

    /**
     * EloquentCountriesRepository constructor.
     *
     * @param Countries $countries
     */
    public function __construct(Countries $countries)
    {
        $this->countries = $countries;
    }

    /**
     * @param array $data
     * @return Countries|Model
     */
    public function create(array $data) {
        return $this->countries->create($data);
    }

    /**
     * @param string $name
     * @return Builder
     */
    public function findByName(string $name) {
        return $this->countries->where('name', '=', $name);
    }
}
