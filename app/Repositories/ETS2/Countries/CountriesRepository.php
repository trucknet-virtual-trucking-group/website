<?php

namespace App\Repositories\ETS2\Countries;

use App\Repositories\BaseRepository;

interface CountriesRepository extends BaseRepository
{
    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param string $name
     * @return mixed
     */
    public function findByName(string $name);
}
