<?php

namespace App\Repositories\ETS2\Game;

use App\Models\ETS2\Game;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class EloquentGameRepository implements GameRepository
{
    /** @var Game */
    protected $game;

    /**
     * EloquentGameRepository constructor.
     *
     * @param Game $game
     */
    public function __construct(Game $game)
    {
        $this->game = $game;
    }

    /**
     * @param array $data
     */
    public function create(array $data) {
        $this->game->create($data);
    }

    /**
     * @param string $name
     * @return Builder|Model|object|null
     */
    public function findByName(string $name) {
        return $this->game->where('shortName', '=', $name)->first();
    }
}
