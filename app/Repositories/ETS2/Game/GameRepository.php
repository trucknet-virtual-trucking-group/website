<?php

namespace App\Repositories\ETS2\Game;

use App\Repositories\BaseRepository;

interface GameRepository extends BaseRepository
{
    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param string $name
     * @return mixed
     */
    public function findByName(string $name);
}
