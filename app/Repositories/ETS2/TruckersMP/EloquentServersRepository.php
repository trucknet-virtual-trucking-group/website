<?php

namespace App\Repositories\ETS2\TruckersMP;

use App\Models\TruckersMP\Servers;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class EloquentServersRepository implements ServersRepository
{
    /** @var Servers */
    protected $servers;

    /**
     * EloquentCitiesRepository constructor.
     * @param Servers $servers
     */
    public function __construct(Servers $servers)
    {
        $this->servers = $servers;
    }

    /**
     * @param array $data
     * @return Servers|Model
     */
    public function create(array $data): Servers
    {
        return $this->servers->create($data);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->servers->all();
    }

    /**
     * @return Servers[]|Collection
     */
    public function ets(): Collection
    {
        return $this->servers->all()->where('game_id', '=', 1);
    }

    /**
     * @return Collection
     */
    public function ats(): Collection
    {
        return $this->servers->all()->where('game_id', '=', 2);
    }

    /**
     * @param string $name
     * @return Builder|Servers|object|null
     */
    public function findByName(string $name)
    {
        return $this->servers->where('name', '=', $name)->first();
    }
}
