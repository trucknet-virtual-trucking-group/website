<?php

namespace App\Repositories\ETS2\TruckersMP;

use App\Models\TruckersMP\Users;
use Illuminate\Database\Eloquent\Collection;

class EloquentUsersRepository implements UsersRepository
{
    /** @var Users */
    protected $users;

    /**
     * EloquentUsersRepository constructor.
     *
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * @param array $data
     * @return Users
     */
    public function create(array $data): Users
    {
        return $this->users->create($data);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->users->all();
    }

    /**
     * @param string $name
     * @return Users
     */
    public function findByName(string $name)
    {
        return $this->users->where('name', '=', $name)->first();
    }
}
