<?php

namespace App\Repositories\ETS2\TruckersMP;

use App\Models\TruckersMP\Users;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;

interface UsersRepository extends BaseRepository
{
    /**
     * @param array $data
     * @return Users
     */
    public function create(array $data);

    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @param string $name
     * @return Users
     */
    public function findByName(string $name);
}
