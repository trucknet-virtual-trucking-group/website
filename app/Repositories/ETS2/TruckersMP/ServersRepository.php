<?php

namespace App\Repositories\ETS2\TruckersMP;

use App\Models\TruckersMP\Servers;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;

interface ServersRepository extends BaseRepository
{
    /**
     * @param array $data
     * @return Servers
     */
    public function create(array $data);

    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @return Collection
     */
    public function ets(): Collection;

    /**
     * @return Collection
     */
    public function ats(): Collection;

    /**
     * @param string $name
     * @return Servers
     */
    public function findByName(string $name);
}
