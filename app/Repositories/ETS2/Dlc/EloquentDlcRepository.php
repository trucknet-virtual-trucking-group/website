<?php

namespace App\Repositories\ETS2\Dlc;

use App\Models\ETS2\DLC;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class EloquentDlcRepository implements DlcRepository
{
    /** @var DLC */
    protected $dlc;

    /**
     * EloquentDlcRepository constructor.
     *
     * @param DLC $dlc
     */
    public function __construct(DLC $dlc)
    {
        $this->dlc = $dlc;
    }

    /**
     * @param array $data
     * @return DLC|Model
     */
    public function create(array $data) {
        return $this->dlc->create($data);
    }

    /**
     * @param string $name
     * @return Builder|Model|object|null
     */
    public function findByName(string $name) {
        return $this->dlc->where('name', '=', $name)->first();
    }
}
