<?php

namespace App\Repositories\ETS2\Dlc;

use App\Repositories\BaseRepository;

interface DlcRepository extends BaseRepository
{
    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * @param string $name
     * @return mixed
     */
    public function findByName(string $name);
}
