<?php

namespace App\Repositories\ETS2\Trucks;

use App\Models\ETS2\Trucks;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Collection;

interface TrucksRepository extends BaseRepository
{
    /**
     * @param array $data
     * @return Trucks
     */
    public function create(array $data): Trucks;

    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @return Trucks[]|Collection
     */
    public function ets(): Collection;

    /**
     * @return Trucks[]|Collection
     */
    public function ats(): Collection;
}
