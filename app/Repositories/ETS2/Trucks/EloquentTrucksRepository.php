<?php

namespace App\Repositories\ETS2\Trucks;

use App\Models\ETS2\Trucks;
use Illuminate\Database\Eloquent\Collection;

class EloquentTrucksRepository implements TrucksRepository
{
    /** @var Trucks */
    protected $trucks;

    /**
     * EloquentTrucksRepository constructor.
     *
     * @param Trucks $trucks
     */
    public function __construct(Trucks $trucks)
    {
        $this->trucks = $trucks;
    }

    /**
     * @param array $data
     * @return Trucks
     */
    public function create(array $data): Trucks
    {
        return $this->trucks->create($data);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->trucks->all();
    }

    /**
     * @return Trucks[]|Collection
     */
    public function ets(): Collection
    {
        return $this->trucks->all()->where('game_id', '=', 1);
    }

    /**
     * @return Trucks[]|Collection
     */
    public function ats(): Collection
    {
        return $this->trucks->all()->where('game_id', '=', 2);
    }
}
