<?php

namespace App\Repositories\ETS2\Cities;

use App\Models\ETS2\Cities;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class EloquentCitiesRepository implements CitiesRepository
{
    /** @var Cities */
    protected $cities;

    /**
     * EloquentCitiesRepository constructor.
     *
     * @param Cities $cities
     */
    public function __construct(Cities $cities)
    {
        $this->cities = $cities;
    }

    /**
     * @param array $data
     * @return Cities|Model
     */
    public function create(array $data) {
        return $this->cities->create($data);
    }

    /**
     * @param string $name
     * @return Builder|Model|object|null
     */
    public function findByName(string $name) {
        return $this->cities->where('name', '=', $name)->first();
    }
}
