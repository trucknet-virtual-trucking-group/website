<?php

namespace App\Repositories\ETS2\Cities;

use App\Models\ETS2\Cities;
use App\Repositories\BaseRepository;

interface CitiesRepository extends BaseRepository
{
    /**
     * @param array $data
     * @return Cities
     */
    public function create(array $data);

    /**
     * @param string $name
     * @return Cities
     */
    public function findByName(string $name);
}
