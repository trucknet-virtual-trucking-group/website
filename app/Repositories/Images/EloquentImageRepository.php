<?php

namespace App\Repositories\Images;

use App\Models\images\Image;

class EloquentImageRepository implements ImageRepository
{
    /** @var Image */
    protected $image;

    /**
     * EloquentImageRepository constructor.
     *
     * @param Image $image
     */
    public function __construct(Image $image)
    {
        $this->image = $image;
    }

    /**
     * @param array $data
     * @return Image
     */
    public function create(array $data)
    {
        return $this->image->create($data);
    }

    /**
     * @param int $id
     * @return Image
     */
    public function getImageById(int $id)
    {
        return $this->image->find($id);
    }

    /**
     * @param int $id
     * @param array $data
     * @return bool
     */
    public function update(int $id, array $data)
    {
        return $this->image->find($id)->update($data);
    }
}
