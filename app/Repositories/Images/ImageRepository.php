<?php

namespace App\Repositories\Images;

use App\Repositories\BaseRepository;
use App\Models\images\Image;

interface ImageRepository extends BaseRepository
{
    /**
     * @param array $data
     * @return Image
     */
    public function create(array $data);

    /**
     * @param int $id
     * @return Image
     */
    public function getImageById(int $id);

    /**
     * @param int $id
     * @param array $data
     * @return bool
     */
    public function update(int $id, array $data);
}
