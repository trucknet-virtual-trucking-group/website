<?php

namespace App\Helpers\Parser;

class Scripts
{
    /**
     * @param string $code
     * @return string|string[]
     */
    public function modifiedScript(string $code)
    {
        return str_replace(['<script>', '</script>'], ['', ''], $code);
    }
}
