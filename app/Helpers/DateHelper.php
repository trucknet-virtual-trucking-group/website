<?php

namespace App\Helpers;

class DateHelper
{
    /**
     * @param $format
     * @param int $timestamp
     * @param bool $nominative_month
     * @return false|string
     */
    public static function ruDate($format, $timestamp = 0, $nominative_month = false)
    {
        if (!$timestamp) $timestamp = time();
        elseif (!preg_match("/^[0-9]+$/", $timestamp)) $timestamp = strtotime($timestamp);

        $F = $nominative_month ? [1 => "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"] : [1 => "Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"];
        $M = [1 => "Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"];
        $l = ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"];
        $D = ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"];

        $format = str_replace("F", $F[date("n", $timestamp)], $format);
        $format = str_replace("M", $M[date("n", $timestamp)], $format);
        $format = str_replace("l", $l[date("w", $timestamp)], $format);
        $format = str_replace("D", $D[date("w", $timestamp)], $format);

        return date($format, $timestamp);
    }
}
