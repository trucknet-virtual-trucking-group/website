<?php

namespace App\Models\History;

use App\Models\Model;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\History\History
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property int|null $staff_id
 * @property string $type
 * @property string|null $from
 * @property string|null $to
 * @property string|null $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History\History newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History\History newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History\History query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History\History whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History\History whereFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History\History whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History\History whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History\History whereStaffId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History\History whereTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History\History whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History\History whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History\History whereUserId($value)
 */
class History extends Model
{
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'history';

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
