<?php

namespace App\Models\TruckersMP;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

/**
 * App\Models\TruckersMP\Servers
 *
 * @property int      $id
 * @property int      $game_id
 * @property bool     $online
 * @property int      $actual_id
 * @property string   $name
 * @property string   $shortName
 * @property Carbon   $created_at
 * @property Carbon   $updated_at
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Servers newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Servers newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TruckersMP\Servers onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Servers query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Servers whereActualId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Servers whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Servers whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Servers whereGameId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Servers whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Servers whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Servers whereOnline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Servers whereShortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Servers whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TruckersMP\Servers withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TruckersMP\Servers withoutTrashed()
 */
class Servers extends Model
{
    use SoftDeletes;

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'servers';

    /** @var array */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * Атрибуты, которые должны быть приведены к собственным типам.
     *
     * @var array
     */
    protected $casts = [
        'speedlimiter'  => 'boolean',
        'online'        => 'boolean',
    ];
}
