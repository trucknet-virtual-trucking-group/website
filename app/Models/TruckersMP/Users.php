<?php

namespace App\Models\TruckersMP;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\TruckersMP\Users
 *
 * @property int $id
 * @property int $user_id
 * @property int $mp_id
 * @property string $name
 * @property string $avatar
 * @property string $smallAvatar
 * @property string $groupName
 * @property int $groupID
 * @property bool $banned
 * @property string|null $bannedUntil
 * @property bool $displayBans
 * @property string|null $bans
 * @property bool $inGameAdmin
 * @property string|null $vtc
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TruckersMP\Users onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users whereBanned($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users whereBannedUntil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users whereBans($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users whereDisplayBans($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users whereGroupID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users whereGroupName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users whereInGameAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users whereMpId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users whereSmallAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TruckersMP\Users whereVtc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TruckersMP\Users withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TruckersMP\Users withoutTrashed()
 * @mixin \Eloquent
 */
class Users extends Model
{
    use SoftDeletes;

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'truckers_mp';

    /** @var array */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * Атрибуты, которые должны быть приведены к собственным типам.
     *
     * @var array
     */
    protected $casts = [];

    protected $fillable = [
        'mp_id',
        'name',
        'avatar',
        'smallAvatar',
        'groupName',
        'groupID',
        'banned',
        'bannedUntil',
        'displayBans',
        'bans',
        'inGameAdmin',
        'vtc',
    ];
}
