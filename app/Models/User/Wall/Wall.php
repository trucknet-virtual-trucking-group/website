<?php

namespace App\Models\User\Wall;

use App\Models\Model;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Wall extends Model
{
    use SoftDeletes;

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'wall';

    /** @var array */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'text',
    ];

    /**
     * Кто создал пост.
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Лайки поста.
     *
     * @return HasMany
     */
    public function likes()
    {
        return $this->hasMany(WallLikes::class);
    }

    /**
     * @return mixed
     */
    public function currentLike()
    {
        return $this->hasOne(WallLikes::class)->userLike(Auth::user()->id);
    }
}
