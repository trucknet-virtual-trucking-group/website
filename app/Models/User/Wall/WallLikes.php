<?php

namespace App\Models\User\Wall;

use App\Models\Model;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\User\Wall\WallLikes
 *
 * @property int $id
 * @property int $wall_id
 * @property int $user_id
 * @property bool $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User\User $user
 * @property-read \App\Models\User\Wall\Wall $wall
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallLikes newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallLikes newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallLikes query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallLikes userLike($user_id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallLikes whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallLikes whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallLikes whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallLikes whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallLikes whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallLikes whereWallId($value)
 * @mixin \Eloquent
 */
class WallLikes extends Model
{
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'wall_likes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'wall_id',
        'user_id',
        'type',
    ];

    /**
     * Автор лайка.
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Пост к которому принадлежит лайк.
     *
     * @return BelongsTo
     */
    public function wall()
    {
        return $this->belongsTo(Wall::class);
    }

    /**
     * @param $q
     * @param $user_id
     * @return mixed
     */
    public function scopeUserLike($q, $user_id)
    {
        return $q->where([['user_id', '=', $user_id], ['type', '=', true]]);
    }
}
