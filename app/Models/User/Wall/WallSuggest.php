<?php

namespace App\Models\User\Wall;

use App\Models\Model;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\User\Wall\WallSuggest
 *
 * @property int $id
 * @property int $user_id
 * @property int $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallSuggest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallSuggest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallSuggest query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallSuggest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallSuggest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallSuggest whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallSuggest whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wall\WallSuggest whereUserId($value)
 * @mixin \Eloquent
 */
class WallSuggest extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
    ];

    /**
     * Get the user for the wall.
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
