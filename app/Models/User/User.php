<?php

namespace App\Models\User;

use App\Models\Auth\SocialAccount;
use App\Models\Images\Image;
use App\Models\TruckersMP\Users;
use App\Models\User\Wall\Wall;
use App\Models\User\Wall\WallSuggest;
use Eloquent;
use HttpOz\Roles\Models\Role;
use HttpOz\Roles\Traits\HasRole;
use HttpOz\Roles\Contracts\HasRole as HasRoleContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\HigherOrderBuilderProxy;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;
use App\Models\User\Settings\UserSettings;
use Illuminate\Support\Facades\Storage;

/**
 * App\Models\User\User
 *
 * @property int $id
 * @property string $email
 * @property string $steam_id
 * @property string $remember_token
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @mixin Eloquent
 * @property string|null $firstName
 * @property string|null $lastName
 * @property string|null $dateOfBirth
 * @property string|null $country
 * @property string|null $city
 * @property int|null $gender
 * @property string|null $login
 * @property string|null $nickname
 * @property string|null $nickname_color
 * @property string|null $email_verified_at
 * @property string|null $password
 * @property string|null $steam_username
 * @property string|null $steam_avatar
 * @property int|null $vk_id
 * @property int $avatar_id
 * @property int $cover_id
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read Collection|Role[] $roles
 * @property-read int|null $roles_count
 * @property-read Collection|Image[] $userImages
 * @property-read int|null $user_images_count
 * @property-read UserSettings|null $userSettings
 * @property-read Users|null $userTruckersMp
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereAvatarId($value)
 * @method static Builder|User whereCity($value)
 * @method static Builder|User whereCountry($value)
 * @method static Builder|User whereCoverId($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereDateOfBirth($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereFirstName($value)
 * @method static Builder|User whereGender($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereLastName($value)
 * @method static Builder|User whereLogin($value)
 * @method static Builder|User whereNickname($value)
 * @method static Builder|User whereNicknameColor($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereSteamAvatar($value)
 * @method static Builder|User whereSteamId($value)
 * @method static Builder|User whereSteamUsername($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @method static Builder|User whereVkId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Auth\SocialAccount[] $accounts
 * @property-read int|null $accounts_count
 * @property-read \App\Models\Images\Image $avatar
 * @property-read \App\Models\Images\Image $cover
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\Wall\WallSuggest[] $wallSuggest
 * @property-read int|null $wall_suggest_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User\Wall\Wall[] $walls
 * @property-read int|null $walls_count
 */
class User extends Authenticatable implements HasRoleContract
{
    use Notifiable, HasRole;

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Первичный ключ
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Атрибуты, которые не могут быть назначены массово.
     *
     * @var array
     */
    protected $guarded = ['remember_token'];

    /**
     * Атрибуты, которые должны быть скрыты для массивов.
     *
     * @var array
     */
    protected $hidden = ['remember_token'];

    /**
     * Атрибуты, которые должны быть приведены к собственным типам.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName',
        'lastName',
        'dateOfBirth',
        'country',
        'city',
        'gender',
        'login',
        'nickname',
        'nickname_color',
        'email',
        'email_verified_at',
        'password',
        'steam_id',
        'steam_username',
        'steam_avatar',
        'vk_id',
    ];

    /**
     * Настройки пользователя
     *
     * @return HasOne
     */
    public function userSettings()
    {
        return $this->hasOne(UserSettings::class);
    }

    /**
     * TruckersMp
     *
     * @return HasOne
     */
    public function userTruckersMp()
    {
        return $this->hasOne(Users::class);
    }

    /**
     * Images
     *
     * @return HasMany
     */
    public function userImages()
    {
        return $this->hasMany(Image::class);
    }

    /**
     * Walls
     *
     * @return HasMany
     */
    public function walls()
    {
        return $this->hasMany(Wall::class);
    }

    /**
     * Wall Suggest
     *
     * @return HasMany
     */
    public function wallSuggest()
    {
        return $this->hasMany(WallSuggest::class);
    }

    /**
     * @return BelongsTo
     */
    public function avatar()
    {
        return $this->belongsTo(Image::class, 'avatar_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function cover()
    {
        return $this->belongsTo(Image::class, 'cover_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function accounts()
    {
        return $this->hasMany(SocialAccount::class);
    }

    /**
     * @return HigherOrderBuilderProxy|mixed|string
     */
    public function getSmallAvatar()
    {
        if (!$this->avatar_id) {
            return '/images/avatar.png';
        }

        return Storage::url($this->avatar->path);
    }

    /**
     * @return HigherOrderBuilderProxy|mixed|string
     */
    public function getCover()
    {
        if (!$this->cover_id) {
            return '/images/cover.png';
        }

        return Storage::url($this->cover->path);
    }

    /**
     * Проверить, если пользователь находится в определенной группе
     *
     * @param $group
     * @return bool
     */
    public function isGroup($group): bool
    {
        return $this->group() === $group;
    }

    /**
     * @return string
     */
    public function fullName(): string
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    /**
     * @return string
     */
    public function timeTheProject()
    {
        $dateNow = Carbon::now();
        $interval = date_diff($dateNow, $this->created_at);
        $hours = $interval->d * 24 + $interval->h;
        $time = $this->declOfNum($interval->d * 24 + $interval->h, ['час', 'часа', 'часов']);

        return $hours . ' ' . $time;
    }

    /**
     * @param $number
     * @param $titles
     * @return mixed
     */
    public function declOfNum($number, $titles)
    {
        $cases = array(2, 0, 1, 1, 1, 2);
        return $titles[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }
}
