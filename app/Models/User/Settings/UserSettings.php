<?php

namespace App\Models\User\Settings;

use Illuminate\Database\Eloquent\Model;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\User\Settings\UserSettings
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $vk_id
 * @property string|null $youtube
 * @property string|null $instagram
 * @property string|null $twitch
 * @property int|null $ats_preferred_server
 * @property int|null $ets_preferred_server
 * @property int|null $ats_preferred_truck
 * @property int|null $ets_preferred_truck
 * @property int|null $truckers_mp_id
 * @property string|null $truckers_mp_username
 * @property string|null $truckers_mp_avatar
 * @property bool $truckers_mp_banned
 * @property bool $rules_accepted
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings whereAtsPreferredServer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings whereAtsPreferredTruck($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings whereEtsPreferredServer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings whereEtsPreferredTruck($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings whereInstagram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings whereRulesAccepted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings whereTruckersMpAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings whereTruckersMpBanned($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings whereTruckersMpId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings whereTruckersMpUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings whereTwitch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings whereVkId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Settings\UserSettings whereYoutube($value)
 * @mixin \Eloquent
 */
class UserSettings extends Model
{
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'users_settings';

    /**
     * Первичный ключ
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Атрибуты для массового заполнения.
     *
     * @var array
     */
    protected $fillable = [
        'firstName',
        'lastName',
        'nickname',
        'tag',
        'tag_color',
        'steam_username',
        'steam_avatar',
        'truckers_mp_id',
        'truckers_mp_username',
        'truckers_mp_avatar',
        'truckers_mp_banned',
        'rules_accepted',
    ];

    /**
     * @return BelongsTo
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * @return string
     */
    public function getAvatar() {
        return $this->steam_avatar ? $this->steam_avatar : $this->truckers_mp_avatar;
    }

    /**
     * @return string
     */
    public function fullName() {
        return $this->firstName . ' ' . $this->lastName;
    }
}
