<?php

namespace App\Models\ETS2;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

/**
 * App\Models\ETS2\DLC
 *
 * @property int        $id
 * @property string     $name
 * @property integer    $game_id
 * @property integer    $order
 * @property Carbon     $created_at
 * @property Carbon     $updated_at
 * @property Carbon     $deleted_at
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\DLC newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\DLC newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ETS2\DLC onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\DLC query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\DLC whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\DLC whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\DLC whereGameId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\DLC whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\DLC whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\DLC whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\DLC whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ETS2\DLC withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ETS2\DLC withoutTrashed()
 */
class DLC extends Model
{
    use SoftDeletes;

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'dlc';
}
