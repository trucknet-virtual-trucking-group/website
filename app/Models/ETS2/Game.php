<?php

namespace App\Models\ETS2;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

/**
 * App\Models\ETS2\Game
 *
 * @property int        $id
 * @property string     $name
 * @property string     $shortName
 * @property Carbon     $created_at
 * @property Carbon     $updated_at
 * @property Carbon     $deleted_at
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Game newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Game newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ETS2\Game onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Game query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Game whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Game whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Game whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Game whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Game whereShortName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Game whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ETS2\Game withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ETS2\Game withoutTrashed()
 */
class Game extends Model
{
    use SoftDeletes;

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'games';
}
