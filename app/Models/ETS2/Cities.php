<?php

namespace App\Models\ETS2;

use App\Models\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\ETS2\Cities
 *
 * @property int        $id
 * @property int        $country_id
 * @property string     $name
 * @property int        $dlc_id
 * @property Carbon     $created_at
 * @property Carbon     $updated_at
 * @property Carbon     $deleted_at
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Cities newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Cities newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ETS2\Cities onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Cities query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Cities whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Cities whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Cities whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Cities whereDlcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Cities whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Cities whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Cities whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ETS2\Cities withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ETS2\Cities withoutTrashed()
 */
class Cities extends Model
{
    use SoftDeletes;

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'cities';
}
