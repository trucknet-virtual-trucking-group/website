<?php

namespace App\Models\ETS2;

use App\Models\Model;

/**
 * App\Models\ETS2\Trucks
 *
 * @property int        $id
 * @property string     $name
 * @mixin \Eloquent
 * @property int $game_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Trucks newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Trucks newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Trucks query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Trucks whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Trucks whereGameId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Trucks whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Trucks whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Trucks whereUpdatedAt($value)
 */
class Trucks extends Model
{
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'trucks';

    /**
     * Атрибуты для массового заполнения.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}
