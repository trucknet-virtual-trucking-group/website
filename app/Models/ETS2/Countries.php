<?php

namespace App\Models\ETS2;

use App\Models\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\ETS2\Countries
 *
 * @property int        $id
 * @property int        $game_id
 * @property string     $name
 * @property int        $dlc_id
 * @property Carbon     $created_at
 * @property Carbon     $updated_at
 * @property Carbon     $deleted_at
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Countries newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Countries newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ETS2\Countries onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Countries query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Countries whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Countries whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Countries whereDlcId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Countries whereGameId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Countries whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Countries whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ETS2\Countries whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ETS2\Countries withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ETS2\Countries withoutTrashed()
 */
class Countries extends Model
{
    use SoftDeletes;

    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'countries';
}
