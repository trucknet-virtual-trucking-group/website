<?php

namespace App\Models\Images;

use App\Models\Model;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Images\Image
 *
 * @property int $id
 * @property int $user_id
 * @property string $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \App\Models\User\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Images\Image newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Images\Image newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Images\Image query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Images\Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Images\Image whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Images\Image whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Images\Image wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Images\Image whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Images\Image whereUserId($value)
 * @mixin \Eloquent
 */
class Image extends Model
{
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'images';

    /**
     * Первичный ключ
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'path',
    ];

    /**
     * Get the user for the image.
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
