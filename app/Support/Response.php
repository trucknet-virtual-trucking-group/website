<?php

namespace App\Support;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use League\Fractal\TransformerAbstract;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class Response
{
    /**
     * HTTP Response.
     *
     * @var ResponseFactory
     */
    private $response;

    /**
     * API transformer helper.
     *
     * @var Transform
     */
    public $transform;

    /**
     * HTTP status code.
     *
     * @var int
     */
    private $httpCode = HttpResponse::HTTP_OK;

    /** @var bool */
    private $status = true;

    /**
     * Create a new class instance.
     *
     * @param $response
     * @param $transform
     */
    public function __construct(ResponseFactory $response, Transform $transform)
    {
        $this->response = $response;
        $this->transform = $transform;
    }

    /**
     * Return a 201 response with the given created resource.
     *
     * @param null $resource
     * @param TransformerAbstract|null $transformer
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function withCreated($resource = null, TransformerAbstract $transformer = null): JsonResponse
    {
        $this->httpCode = HttpResponse::HTTP_CREATED;

        if ($resource === null) {
            return $this->json();
        }

        return $this->item($resource, $transformer);
    }

    /**
     * @return JsonResponse
     */
    public function withNoContent(): JsonResponse
    {
        return $this->setHttpCode(HttpResponse::HTTP_NO_CONTENT)->json();
    }

    /**
     * Make a 400 'Bad Request' response.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function withBadRequest($message = 'Bad Request'): JsonResponse
    {
        return $this->setHttpCode(HttpResponse::HTTP_BAD_REQUEST)->withError($message);
    }

    /**
     * Make a 401 'Unauthorized' response.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function withUnauthorized($message = 'Unauthorized'): JsonResponse
    {
        return $this->setHttpCode(HttpResponse::HTTP_UNAUTHORIZED)->withError($message);
    }

    /**
     * Make a 403 'Forbidden' response.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function withForbidden($message = 'Server: Forbidden'): JsonResponse
    {
        return $this->setHttpCode(HttpResponse::HTTP_FORBIDDEN)->withError($message);
    }

    /**
     * Make a 404 'Not Found' response.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function withNotFound($message = 'Not Found'): JsonResponse
    {
        return $this->setHttpCode(HttpResponse::HTTP_NOT_FOUND)->withError($message);
    }

    /**
     * Make a 405 'Method Not Allow' response.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function withMethodNotAllow($message = 'Method Not Allow'): JsonResponse
    {
        return $this->setHttpCode(HttpResponse::HTTP_METHOD_NOT_ALLOWED)->withError($message);
    }

    /**
     * Make a 429 'Too Many Requests' response.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function withTooManyRequests($message = 'Too Many Requests'): JsonResponse
    {
        return $this->setHttpCode(HttpResponse::HTTP_TOO_MANY_REQUESTS)->withError($message);
    }

    /**
     * Make a 500 'Internal Server Error' response.
     *
     * @param string $message
     *
     * @return JsonResponse
     */
    public function withInternalServer($message = 'Internal Tengine Server Error'): JsonResponse
    {
        return $this->setHttpCode(HttpResponse::HTTP_INTERNAL_SERVER_ERROR)->withError($message);
    }

    /**
     * Make an error response.
     *
     * @param string $message
     *
     * @param string $field_id
     * @return JsonResponse
     */
    public function withError($message = 'Something Error', $field_id = ''): JsonResponse
    {
        return $this->response->json(['response' => 0, 'error_msg' => $message, 'field_id' => $field_id]);
    }

    /**
     * Make a JSON response with the transformed items.
     *
     * @param $item
     * @param TransformerAbstract|null $transformer
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function item($item, TransformerAbstract $transformer = null): JsonResponse
    {
        return $this->json(['response' => 1, 'data' => $this->transform->item($item, $transformer)]);
    }

    /**
     * Make a JSON response.
     *
     * @param $items
     * @param TransformerAbstract|null $transformer
     * @return JsonResponse
     *@throws \Exception
     */
    public function collection($items, TransformerAbstract $transformer = null): JsonResponse
    {
        return $this->json(['response' => 1, 'data' => $this->transform->collection($items, $transformer)]);
    }

    /**
     * @param array $data
     * @param array $headers
     * @return JsonResponse
     */
    public function json($data = [], array $headers = []): JsonResponse
    {
        return $this->response->json(['response' => 1, 'data' => $data], $this->httpCode, $headers);
    }

    /**
     * Set HTTP status code.
     *
     * @param int $httpCode
     *
     * @return $this
     */
    public function setHttpCode($httpCode): self
    {
        $this->httpCode = $httpCode;
        if ($httpCode >= 300 || $httpCode < 200) {
            $this->status = false;
        }
        return $this;
    }

    /**
     * Gets the HTTP status code.
     *
     * @return int
     */
    public function getHttpCode(): int
    {
        return $this->httpCode;
    }
}
