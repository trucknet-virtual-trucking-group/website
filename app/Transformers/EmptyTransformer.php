<?php

namespace App\Transformers;

class EmptyTransformer
{
    /**
     * @return array
     */
    public function transform(): array
    {
        return [];
    }
}
