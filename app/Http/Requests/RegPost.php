<?php

namespace App\Http\Requests;

class RegPost extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:3|max:20',
            'last_name' => 'required|min:3|max:20',
            'login' => 'required|unique:users|min:4|max:20',
            'password' => 'required|min:6|max:35',
            'email' => 'required|email|unique:users,email',
            'captcha_code' => 'required|min:6|captcha_api:'. request('captcha_key'),
            'steam_id' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'first_name.required' => 'Укажите ваше Имя.',
            'first_name.min' => 'Ваше имя слишком короткое.',
            'first_name.max' => 'Ваше имя слишком длинное.',
            'last_name.required' => 'Укажите вашу Фамилию.',
            'last_name.min' => 'Ваша фамилия слишком короткое.',
            'last_name.max' => 'Ваша фамилия слишком длинное.',
            'login.required' => 'Укажите ваш логин.',
            'login.unique' => 'Логин уже занят.',
            'login.min' => 'Логин слишком короткий.',
            'login.max' => 'Логин слишком длинный.',
            'password.required' => 'Укажите ваш пароль.',
            'password.min' => 'Пароль слишком короткий.',
            'password.max' => 'Пароль слишком длинный.',
            'email.required' => 'Укажите ваш email.',
            'email.email' => 'Email выглядит не так.',
            'email.unique' => 'Email уже зарегистрирован.',
            'captcha_code.required' => 'Укажите код с картинки.',
            'captcha_code.min' => 'Код с картинки слишком короткий.',
            'captcha_code.captcha_api' => 'Неверный код с картинки.',
            'steam_id.required' => 'Повторите регистрацию.',
        ];
    }
}
