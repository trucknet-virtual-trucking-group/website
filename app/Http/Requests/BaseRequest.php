<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class BaseRequest extends FormRequest
{
    /**
     * Handle a failed validation attempt.
     *
     * @param Validator $validator
     * @return mixed
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(new JsonResponse($this->formatErrors($validator), 200));
    }

    /**
     * Format the errors from the given Validator instance.
     *
     * @param Validator $validator
     * @return array
     */
    protected function formatErrors(Validator $validator)
    {
        $array_errors = $validator->getMessageBag()->toArray();

        $field_name = array_keys($array_errors);
        $error_msg = array_shift($array_errors);

        return ['response' => 0, 'error_msg' => $error_msg[0], 'field_id' => $field_name[0]];
    }
}
