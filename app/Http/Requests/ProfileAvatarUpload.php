<?php

namespace App\Http\Requests;

class ProfileAvatarUpload extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !auth()->guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required|mimes:jpeg,jpg,png|dimensions:min_width=250,min_height=250',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'file.required' => 'Пожалуйста выберите файл',
            'file.mimes' => 'Формат изображения должен быть: jpeg, jpg, png',
            'file.dimensions' => 'Минимальная ширина и высота 250',
        ];
    }
}
