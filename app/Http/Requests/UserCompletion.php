<?php

namespace App\Http\Requests;

class UserCompletion extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !auth()->guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => 'required|unique:users|min:4|max:20',
            'password' => 'required|min:6|max:35',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'login.required' => 'Укажите ваш логин.',
            'login.unique' => 'Логин уже занят.',
            'login.min' => 'Логин слишком короткий.',
            'login.max' => 'Логин слишком длинный.',
            'password.required' => 'Укажите ваш пароль.',
            'password.min' => 'Пароль слишком короткий.',
            'password.max' => 'Пароль слишком длинный.',
        ];
    }
}
