<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User\User;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class DashboardController extends Controller
{
    use SEOToolsTrait;

    /**
     * @return Factory|View
     */
    public function index()
    {
        $this->seo()->setTitle('Панель управления');

        $users_count  = User::count();

        return view('admin.dashboard.index', compact('users_count'));
    }
}
