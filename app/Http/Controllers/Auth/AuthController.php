<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\User\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Auth\SocialAccount;
use Illuminate\Validation\ValidationException;
use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\HttpFoundation\RedirectResponse;
use VK\Exceptions\VKApiException;
use VK\Exceptions\VKClientException;

class AuthController extends Controller
{
    /** @var UserService */
    protected $userService;

    /**
     * AuthController constructor.
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Redirect the user to the GitHub authentication.
     *
     * @param $provider
     * @return RedirectResponse
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from social.
     *
     * @param $provider
     * @return \Illuminate\Http\RedirectResponse
     * @throws VKApiException
     * @throws VKClientException
     */
    public function handleProviderCallback($provider)
    {
        $providerUser = Socialite::with($provider)->user();

        $account = SocialAccount::where('provider_name', $provider)->where('provider_id', $providerUser->getId())->first();

        if ($account) {
            auth()->login($account->user, true);
            return redirect()->route('user.profile', $account->user->id);
        }

        if ($providerUser->getEmail()) {
            $findByEmailAndAuth = $this->userService->findByEmailAndAuth($providerUser->getEmail());

            if ($findByEmailAndAuth) {
                return redirect()->route('news');
            }
        }

        if ($provider === 'vkontakte') {
            $create_user = $this->userService->createByVk($providerUser);
        }

        if ($provider === 'odnoklassniki') {
            $create_user = $this->userService->createByOk($providerUser);
        }

        if ($provider === 'yandex') {
            $create_user = $this->userService->createByYandex($providerUser);
        }

        if ($provider === 'Google') {
            $create_user = $this->userService->createByGoogle($providerUser);
        }

        if ($create_user->id) {
            SocialAccount::create(['user_id' => $create_user->id, 'provider_name' => $provider, 'provider_id' => $providerUser->getId()]);

            return redirect()->route('news');
        }

        return redirect()->route('index');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'login' => 'required',
            'password' => 'required'
        ]);

        $result = $this->userService->login($request);

        return response()->json($result);
    }
}
