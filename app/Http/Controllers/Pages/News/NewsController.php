<?php

namespace App\Http\Controllers\Pages\News;

use App\Http\Controllers\Controller;
use App\Models\User\Wall\Wall;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class NewsController extends Controller
{
    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $walls = Wall::with(['user' => function ($q) {
            $q->with('userImages', 'avatar');
        }, 'currentLike'])->withCount(['likes' => function ($q) {
            $q->where('type', true);
        }])->orderBy('created_at', 'DESC')->paginate(15);

        return view('pages.news.index', compact('walls'));
    }
}
