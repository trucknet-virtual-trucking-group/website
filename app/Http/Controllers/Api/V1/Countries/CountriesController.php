<?php

namespace App\Http\Controllers\Api\V1\Countries;

use App\Http\Controllers\Api\V1\BaseController;
use App\Services\VkApi\VKApiService;
use Illuminate\Http\JsonResponse;
use VK\Exceptions\VKApiException;
use VK\Exceptions\VKClientException;

class CountriesController extends BaseController
{
    /** @var VKApiService */
    protected $vkApiService;

    /**
     * CountriesController constructor.
     * @param VKApiService $vkApiService
     */
    public function __construct(VKApiService $vkApiService)
    {
        parent::__construct();
        $this->vkApiService = $vkApiService;
    }

    /**
     * @return JsonResponse
     * @throws VKApiException
     * @throws VKClientException
     */
    public function countries()
    {
        return $this->response->json($this->vkApiService->getCountries());
    }
}
