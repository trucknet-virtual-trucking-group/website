<?php

namespace App\Http\Controllers\Api\V1\Captcha;

use App\Http\Controllers\Api\V1\BaseController;
use Intervention\Image\ImageManager;
use Mews\Captcha\Captcha;

class CaptchaController extends BaseController
{
    /**
     * @var Captcha
     */
    protected $captcha;

    /**
     * CaptchaController constructor.
     *
     * @param Captcha $captcha
     */
    public function __construct(Captcha $captcha)
    {
        parent::__construct();

        $this->captcha = $captcha;
    }

    /**
     * @return array|ImageManager|mixed
     * @throws \Exception
     */
    public function create()
    {
        return $this->captcha->create('flat', true);
    }
}
