<?php

namespace App\Http\Controllers\Api\V1\Cities;

use App\Http\Controllers\Api\V1\BaseController;
use App\Services\VkApi\VKApiService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use VK\Exceptions\VKApiException;
use VK\Exceptions\VKClientException;

class CitiesController extends BaseController
{
    /** @var VKApiService */
    protected $vkApiService;

    /**
     * CitiesController constructor.
     * @param VKApiService $vkApiService
     */
    public function __construct(VKApiService $vkApiService)
    {
        parent::__construct();
        $this->vkApiService = $vkApiService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws VKApiException
     * @throws VKClientException
     */
    public function cities(Request $request)
    {
        $country_id = $request->get('country_id');
        $q = $request->get('q');
        return $this->response->json($this->vkApiService->getCities($country_id, $q));
    }
}
