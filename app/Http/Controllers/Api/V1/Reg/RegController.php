<?php

namespace App\Http\Controllers\Api\V1\Reg;

use App\Http\Controllers\Api\V1\BaseController;
use App\Repositories\User\UserRepository;
use App\Services\User\UserService;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class RegController extends BaseController
{
    /** @var UserRepository */
    protected $userRepository;

    /** @var UserService */
    protected $userService;

    /** @var string */
    const U_LOGIN_URL = 'http://ulogin.ru/token.php?token=';

    /**
     * RegController constructor.
     *
     * @param UserRepository $userRepository
     * @param UserService $userService
     */
    public function __construct(UserRepository $userRepository, UserService $userService)
    {
        parent::__construct();

        $this->userRepository = $userRepository;
        $this->userService = $userService;
    }

    /**
     * @param Request $request
     * @return Application|RedirectResponse|Redirector
     * @throws \Exception
     */
    public function reg(Request $request)
    {
        $_user = json_decode(file_get_contents(self::U_LOGIN_URL . $request->get('token') . '&host=' . $_SERVER['HTTP_HOST']), true);

        $error_title = 'Произошла ошибка.';
        $error_msg = 'Данная страница уже зарегистрирована на проекте, пожалуйста авторизируйтесь.';

        if ($_user['network'] === 'vkontakte') {
            $search_vk = $this->userRepository->findByVkId((int) $_user['uid']);

            if ($search_vk) {
                return redirect()->route('index')->with('alert.title', $error_title)->with('alert.message', $error_msg);
            }
        }

        if ($_user['network'] === 'steam') {
            $search_steam = $this->userRepository->findBySteam((int) $_user['uid']);

            if ($search_steam) {
                return redirect()->route('index')->with('alert.title', $error_title)->with('alert.message', $error_msg);
            }
        }

        if ($this->userRepository->findByEmail($_user['email'])) {
            return redirect()->route('index')->with('alert.title', $error_title)->with('alert.message', $error_msg);
        }

        $user_data = [
            'firstName' => $_user['first_name'],
            'lastName' => $_user['last_name'],
            'country' => $_user['country'],
            'city' => $_user['city'],
            'gender' => (int)$_user['sex'],
            'email' => $_user['email'],
            'dateOfBirth' => Carbon::parse($_user['bdate'])->toDate(),
        ];

        if ($_user['network'] === 'vkontakte') {
            $user_data['vk_id'] = (int) $_user['uid'];
        }

        if ($_user['network'] === 'steam') {
            $user_data['steam_id'] = (int) $_user['uid'];
            $user_data['steam_username'] = $_user['nickname'];
            $user_data['steam_avatar'] = $_user['photo_big'];
        }

        $new_user = $this->userRepository->create($user_data);

        if ($_user['network'] === 'steam') {
            $this->userService->attachTruckersMp($new_user);
        }

        auth()->login($new_user, true);

        if ($_user['photo_big'] && $_user['photo']) {
            $photo_big = Image::make($_user['photo_big'])->stream('jpg', 100);
            $photo_big_name = Str::random(40) . '.jpg';

            Storage::disk('sftp')->put('images/users/' . $request->user()->id . '/avatars/' . $photo_big_name, $photo_big);

            $create_image = $new_user->userImages()->create([
                'path' => 'images/users/' . $request->user()->id . '/avatars/' . $photo_big_name,
            ]);

            if ($create_image) {
                $this->userRepository->update($new_user->id, ['avatar_id' => $create_image->id]);
            }
        }

        return redirect()->route('user.profile', $new_user->id);
    }
}
