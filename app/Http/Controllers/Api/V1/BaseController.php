<?php

namespace App\Http\Controllers\Api\V1;

use League\Fractal\Manager;
use App\Http\Controllers\Controller;
use App\Support\Response;
use App\Support\Transform;

class BaseController extends Controller
{
    /** @var Response */
    protected $response;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $manager = new Manager();
        $this->response = new Response(response(), new Transform($manager));
    }
}
