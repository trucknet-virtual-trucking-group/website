<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Api\V1\BaseController;
use App\Http\Requests\ApiAuthPost;
use App\Repositories\User\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class AuthController extends BaseController
{
    /** @var UserRepository */
    protected $userRepository;

    /**
     * AuthController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();

        $this->userRepository = $userRepository;
    }

    /**
     * @param ApiAuthPost $request
     * @return JsonResponse
     */
    public function auth(ApiAuthPost $request)
    {
        $login = $request->get('login');
        $password = $request->get('password');

        $user = $this->userRepository->findByLogin($login);

        if ($user) {
            if (Hash::check($password, $user->password)) {
                Auth::login($user);
                return $this->response->json();
            }

            return $this->response->withError('Неверный пароль.', 'upassword');
        }

        return $this->response->withError('Аккаунт не существует.', 'ulogin');
    }
}
