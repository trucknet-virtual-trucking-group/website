<?php

namespace App\Http\Controllers\Completion;

use App\Http\Controllers\Controller;
use App\Repositories\ETS2\TruckersMP\ServersRepository;
use App\Repositories\ETS2\Trucks\TrucksRepository;
use App\Repositories\User\UserRepository;
use App\Services\VkApi\VKApiService;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use App\Services\User\UserService;
use VK\Exceptions\VKApiException;
use VK\Exceptions\VKClientException;

class CompletionController extends Controller
{
    /** @var ServersRepository */
    protected $serversRepository;

    /** @var TrucksRepository */
    protected $trucksRepository;

    /** @var UserService */
    protected $userService;

    /** @var UserRepository */
    protected $userRepository;

    /** @var VKApiService */
    protected $vKApiService;

    /**
     * CompletionController constructor.
     *
     * @param ServersRepository $serversRepository
     * @param TrucksRepository $trucksRepository
     * @param UserService $userService
     * @param UserRepository $userRepository
     * @param VKApiService $vKApiService
     */
    public function __construct(
        ServersRepository $serversRepository,
        TrucksRepository $trucksRepository,
        UserService $userService,
        UserRepository $userRepository,
        VKApiService $vKApiService
    )
    {
        $this->middleware('auth', ['except' => 'CheckCompletion']);
        $this->serversRepository = $serversRepository;
        $this->trucksRepository = $trucksRepository;
        $this->userService = $userService;
        $this->userRepository = $userRepository;
        $this->vKApiService = $vKApiService;
    }

    /**
     * Завершение регистрации
     *
     * @return Factory|View
     */
    public function index()
    {
        SEOTools::setTitle('Завершение регистрации');

        $servers = $this->serversRepository->all();

        $trucks = $this->trucksRepository->all();

        return view('pages.completion.index', compact(['servers', 'trucks']));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws VKApiException
     * @throws VKClientException
     */
    public function handle(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);

        $user_id = $request->user()->id;

        $userData = $request->only(['email', 'password']);
        $userData['password'] = Hash::make($userData['password']);

        if (!$this->userRepository->update($user_id, $userData)) {
            return response()->json(['status' => 'error', 'error_msg' => 'Произошла ошибка при обновление пользователя.']);
        }

        $requestExcept = ['email', 'password', 'password_confirmation'];

        $userSettings = [];

        foreach ($request->except($requestExcept) as $key => $value) {
            if (!empty($value)) {
                $userSettings[$key] = $value;
            }
        }

        if ($userSettings['vk_id'] !== null) {
            try {
                $newUrl = $this->vKApiService->getUser($userSettings['vk_id']);

                if (isset($newUrl[0]['id'])) {
                    $userSettings['vk_id'] = $newUrl[0]['id'];
                } else {
                    return response()->json(['status' => 'error', 'error_msg' => 'Стараница Вконтакте не найдена.']);
                }
            } catch (\Exception $e) {
                return response()->json(['status' => 'error', 'error_msg' => 'Стараница Вконтакте не найдена.']);
            }
        }

        if (!$this->userRepository->updateSettings($user_id, $userSettings)) {
            return response()->json(['status' => 'error', 'error_msg' => 'Произошла ошибка при обновление настроек пользователя.']);
        }

        return response()->json(['status' => 'success']);
    }
}
