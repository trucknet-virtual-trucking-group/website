<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class MainController extends Controller
{
    /**
     * @param Request $request
     * @return RedirectResponse|View
     */
    public function index(Request $request)
    {
        if (!auth()->guest())
        {
            return redirect()->route('user.profile', auth()->id());
        }

        return view('pages.authorization.index');
    }
}
