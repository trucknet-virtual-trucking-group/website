<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Illuminate\Support\Facades\Redis;

class RestoreController extends Controller
{
    /** @var string */
    const U_LOGIN_URL = 'http://ulogin.ru/token.php?token=';

    /** @var UserRepository */
    protected $userRepository;

    /**
     * RestoreController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     * @return Factory|View|void
     */
    public function index(Request $request)
    {
        $token = $request->get('token');

        if ($token) {
            $key_data = Redis::get('unique_key_restore' . $token);
            if ($key_data) {
                return view('pages.restore.index', compact('token'));
            }
        }

        return view('pages.restore.index');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function restore(Request $request)
    {
        $token = $request->get('token');
        $emailOrLogin = $request->get('login');

        if ($token) {
            $_user = json_decode(file_get_contents(self::U_LOGIN_URL . $token . '&host=' . $_SERVER['HTTP_HOST']), true);

            if ($_user['network'] === 'vkontakte') {
                $user = $this->userRepository->findByVkId((int) $_user['uid']);
            }

            if ($_user['network'] === 'steam') {
                $user = $this->userRepository->findBySteam((int) $_user['uid']);
            }

            if ($user) {
                $token = Str::random(20);

                Redis::set('unique_key_restore' . $token, json_encode(['user_id' => $user->id]));
                Redis::expire('unique_key_restore' . $token, 1200);

                return response()->json(['response' => 1, 'token' => $token]);
            }
        }

        if(preg_match("/^[a-zA-Z0-9_\.\-]+@([a-zA-Z0-9\-]+\.)+[a-zA-Z]{2,6}$/i", $emailOrLogin)) {
            $user = $this->userRepository->findByEmail($emailOrLogin);
        } else {
            $user = $this->userRepository->findByLogin($emailOrLogin);
        }

        // Отправка ссылки на почту.
        if ($user) {
            dd(1);
        }

        return response()->json(['response' => 0, 'error_msg' => 'Пользователь не найден.']);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function newPass(Request $request)
    {
        $token = $request->get('token');
        $new_password = $request->get('new_password');
        $redis_token = Redis::get('unique_key_restore' . $token);

        if ($redis_token) {
            $redis_token_data = json_decode($redis_token);

            $user = $this->userRepository->getUserById($redis_token_data->user_id);
            $user->update(['password' => Hash::make($new_password),]);

            Redis::del('unique_key_restore' . $token);

            auth()->login($user, true);

            return response()->json(['response' => 1]);
        }

        return response()->json(['response' => 0, 'error_msg' => 'Произошла ошибка, попробуйте позже.']);
    }
}
