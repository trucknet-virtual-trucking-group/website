<?php

namespace App\Http\Controllers\User;

use App\Helpers\DateHelper;
use App\Http\Requests\ProfileAvatarUpload;
use App\Http\Requests\ProfileCoverUpload;
use App\Http\Requests\UserCompletion;
use App\Repositories\User\UserRepository;
use App\Services\Images\ImageService;
use Artesaos\SEOTools\Facades\SEOMeta;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\View\Factory;
use App\Http\Controllers\Controller;
use App\Services\User\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;
use App\Models\User\User;

class UserController extends Controller
{
    /** @var UserService */
    protected $userService;

    /** @var UserRepository */
    protected $userRepository;

    /** @var ImageService */
    protected $imageService;

    /**
     * UserController constructor.
     *
     * @param UserService $userService
     * @param UserRepository $userRepository
     * @param ImageService $imageService
     */
    public function __construct(UserService $userService, UserRepository $userRepository, ImageService $imageService)
    {
        $this->userService = $userService;
        $this->userRepository = $userRepository;
        $this->imageService = $imageService;
    }

    /**
     * @param int $id
     * @return Factory|View|void
     */
    public function show(int $id)
    {
        if (Auth::user()->id === $id) {
            $user = Auth::user()->load(['walls' => function ($q) {
                $q->orderBy('created_at', 'DESC');
                $q->limit(15);
                $q->with(['user' => function ($q) {
                    $q->with('userImages', 'avatar');
                }, 'currentLike']);
                $q->withCount(['likes' => function($q){
                    $q->where('type', true);
                }]);
            }]);

            SEOMeta::setTitle($user->fullName());

            if ($user->dateOfBirth) {
                $birth = Carbon::createFromDate($user->dateOfBirth);
                $dateOfBirth = $birth->day . ' ' . DateHelper::ruDate('F', $birth->getTimestamp()) . ', ' . $birth->year;
            } else {
                $dateOfBirth = null;
            }

            $walls = $user->walls;

            return view('pages.profile.profile', compact(['user', 'dateOfBirth', 'walls']));
        }

        $user = User::with(['walls' => function ($q) {
            $q->orderBy('created_at', 'DESC');
            $q->limit(15);
            $q->with(['user' => function ($q) {
                $q->with('userImages', 'avatar');
            }]);
            $q->withCount(['likes' => function($q) {
                $q->where('type', '=', true);
            }]);
        }])->findOrFail($id);

        SEOMeta::setTitle($user->fullName());

        $birth = Carbon::createFromDate($user->dateOfBirth);
        $dateOfBirth = $birth->day . ' ' . DateHelper::ruDate('F', $birth->getTimestamp()) . ', ' . $birth->year;
        $walls = $user->walls;

        return view('pages.profile.profile', compact(['user', 'dateOfBirth', 'walls']));
    }

    /**
     * @param UserCompletion $request
     * @return JsonResponse
     */
    public function completion(UserCompletion $request)
    {
        $update_user = $this->userRepository->update($request->user()->id, [
            'login' => $request->get('login'),
            'password' => Hash::make($request->get('password')),
        ]);

        if ($update_user) {
            return response()->json(['response' => 1]);
        }

        return response()->json(['response' => 0, 'error_msg' => 'Произошла ошибка, попробуйте позже.']);
    }

    /**
     * @param ProfileAvatarUpload $request
     * @return JsonResponse
     */
    public function uploadAvatar(ProfileAvatarUpload $request) {
        $image_tmp = $request->file('file');
        $fileName = 'images/users/' . $request->user()->id . '/avatars/' . Str::random(40) . '.jpg';

        $image = Image::make($image_tmp);
        $image->stream('jpg', 100);

        Storage::disk('public')->put($fileName, $image);

        $create_image = $request->user()->userImages()->create(['path' => $fileName,]);

        if ($create_image) {
            $this->userRepository->update($request->user()->id, ['avatar_id' => $create_image->id]);
        }

        if ($create_image->id) {
            return response()->json(['response' => 1, 'avatar_src' => Storage::url($create_image->path)]);
        }

        return response()->json(['response' => 0, 'error_msg' => 'Произошла ошибка при обновлении фотографий, попробуйте еще раз.']);
    }

    /**
     * @param ProfileCoverUpload $request
     * @return JsonResponse
     */
    public function uploadCover(ProfileCoverUpload $request)
    {
        $image_tmp = $request->file('file');
        $fileName = 'images/users/' . $request->user()->id . '/covers/' . Str::random(40) . '.jpg';

        $image = Image::make($image_tmp);
        $image->stream('jpg', 100);

        $upload = Storage::disk('public')->put($fileName, $image);

        if ($upload) {
            $create_image = $request->user()->userImages()->create(['path' => $fileName,]);

            if ($create_image) {
                $this->userRepository->update($request->user()->id, ['cover_id' => $create_image->id]);

                return response()->json(['response' => 1, 'cover_src' => Storage::url($create_image->path)]);
            }
        }

        return response()->json(['response' => 0, 'error_msg' => 'Произошла ошибка при обновлении обложки, попробуйте еще раз.']);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteCover(Request $request)
    {
        $user = $request->user();

        if (!$user->cover_id) {
            return response()->json(['response' => 0, 'error_msg' => 'Нельзя удалить обложку которой нету.']);
        }

        $model = new \App\Models\Images\Image();
        $img_cover_find = $model::find($user->cover_id);

        Storage::delete(storage_path('app/public/' . $img_cover_find->path));

        $this->userRepository->update($user->id, ['cover_id' => 0]);

        $img_cover_find->delete();

        return response()->json(['response' => 1]);
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Redirector
     */
    public function logout(Request $request) {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect()->route('index');
    }

    /**
     * @return Guard|StatefulGuard|mixed
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
