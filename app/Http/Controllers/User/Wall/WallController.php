<?php

namespace App\Http\Controllers\User\Wall;

use App\Http\Controllers\Controller;
use App\Models\User\Wall\Wall;
use App\Models\User\Wall\WallLikes;
use Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class WallController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function create(Request $request)
    {
        $message = $request->get('message');

        $create_wall = Auth::user()->walls()->create(['text' => $message]);

        if ($create_wall->id) {
            $wall = $create_wall;
            return response()->json(['response' => 1, 'html' => view('layouts.partials.wall', compact('wall'))->render()]);
        }

        return response()->json(['response' => 0, 'error_msg' => 'Произошла ошибка, попробуйте позже.']);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws \Exception
     */
    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:wall,id',
        ]);

        $wall = Wall::find($request->get('id'));

        if (Auth::id() == $wall->user_id) {
            $wall->delete();

            return response()->json(['response' => 1]);
        }

        return response()->json(['response' => 0, 'error_msg' => 'Произошла ошибка, попробуйте позже.']);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function ajaxPagination(Request $request)
    {
        $user_id = $request->get('user_id');

        if ($user_id) {
            $walls = Wall::where('user_id', '=', $user_id)->with(['user' => function ($q) {
                $q->with('userImages', 'avatar');
            }, 'currentLike'])->withCount(['likes' => function ($q) {
                $q->where('type', true);
            }])->orderBy('created_at', 'DESC')->paginate(15);
        } else {
            $walls = Wall::with(['user' => function ($q) {
                $q->with('userImages', 'avatar');
            }, 'currentLike'])->withCount(['likes' => function ($q) {
                $q->where('type', true);
            }])->orderBy('created_at', 'DESC')->paginate(15);
        }

        return response()->json(['response' => 1, 'html' => view('layouts.partials.ajaxWall', compact('walls'))->render()]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function like(Request $request)
    {
        $type = $request->get('type');
        $wall_id = $request->get('wall_id');
        $user_id = $request->user()->id;

        $wall = Wall::find($wall_id);

        if ($wall === null) {
            return response()->json(['response' => 0, 'error_msg' => 'Произошла ошибка, попробуйте позже.']);
        }

        $wall_user_like = WallLikes::where('wall_id', '=', $wall_id)->where('user_id', '=', $user_id)->first();

        if ($wall_user_like === null) {
            $wall->likes()->create(['wall_id' => $wall->id, 'user_id' => $user_id, 'type' => true]);

            return response()->json(['response' => 1]);
        }

        if ($wall_user_like->type) {
            $wall_user_like->type = false;
            $wall_user_like->save();

            return response()->json(['response' => 1]);
        } else {
            $wall_user_like->type = true;
            $wall_user_like->save();

            return response()->json(['response' => 1]);
        }

        return response()->json(['response' => 0, 'error_msg' => 'Произошла ошибка, попробуйте позже.']);
    }
}
