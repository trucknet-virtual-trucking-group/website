<?php

namespace App\Http\Controllers\User\Wall;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class WallSuggestController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function hide(Request $request)
    {
        $type = (int) $request->get('type');
        $current_user = Auth::user();

        $current_user->wallSuggest()->updateOrCreate(['user_id' => $current_user->id, 'type' => $type]);

        return response()->json(['response' => 1]);
    }
}
