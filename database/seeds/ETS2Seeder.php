<?php

use App\Models\ETS2\DLC;
use Illuminate\Database\Seeder;
use App\Models\ETS2\Game;
use App\Models\ETS2\Countries;
use App\Models\ETS2\Cities;
use App\Models\TruckersMP\Servers;
use App\Models\ETS2\Trucks;
use App\Models\ATS\TrucksAts;

class ETS2Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        // Добавляем игры.
        if ($this->gameSeed()) {
            echo "\nИгры были добавлены.\n";
        }

        // Добавляем грузовики ETS2
        if ($this->trucksEtsSeed()) {
            echo "\nГрузовики для ETS2 были добавлены.\n";
        }

        // Добавляем DLC
        if ($this->dlcSeed()) {
            echo "\nDLC были добавлены.\n";
        } else {
            echo "\nDLC не были добавлены!\n";
        }

        // Добавляем страны
        if ($this->countryAndCitySeed()) {
            echo "\nСтраны и Города были добавлены.\n";
        } else {
            echo "\nСтраны и Города не были добавлены!\n";
        }

        // Добавляем сервера truckersMP
        if ($this->truckersMPSeed()) {
            echo "\nСервера truckersMP были добавлены.\n";
        } else {
            echo "\nСервера truckersMP не были добавлены!\n";
        }
    }

    /**
     * @return bool
     */
    private function gameSeed(): bool
    {
        $games = [
            [
                'name' => 'Euro Truck Simulator 2',
                'shortName' => 'ETS2',
            ],
            [
                'name' => 'American Truck Simulator',
                'shortName' => 'ATS',
            ],
        ];

        foreach ($games as $game) {
            if (!Game::where(['shortName' => $game['shortName']])->count()) {
                $d = new Game();
                $d->name = $game['name'];
                $d->shortName = $game['shortName'];
                $d->save();

                echo "\nДобавлена новая игра: " . $game['shortName'] . "\n";
            } else {
                echo "\nИгра" . $game['shortName'] . "уже существует. \n";
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    private function trucksEtsSeed(): bool
    {
        $trucks = [
            [
                'name' => 'Daf XF 105 (2005 г.)',
                'game_id' => 1,
            ],
            [
                'name' => 'Daf XF Euro 6 (2013 г.)',
                'game_id' => 1,
            ],
            [
                'name' => 'Iveco Stralis (2002 г.)',
                'game_id' => 1,
            ],
            [
                'name' => 'Iveco Stralis Hi-Way (2013 г.)',
                'game_id' => 1,
            ],
            [
                'name' => 'Man TGX (2008 г.)',
                'game_id' => 1,
            ],
            [
                'name' => 'Man TGX Euro6 (2016 г.)',
                'game_id' => 1,
            ],
            [
                'name' => 'Mercedes-Benz Actros MP3 (2009 г.)',
                'game_id' => 1,
            ],
            [
                'name' => 'Mercedes-Benz Actros MP4 (2014 г.)',
                'game_id' => 1,
            ],
            [
                'name' => 'Renault Magnum IV (2005 г.)',
                'game_id' => 1,
            ],
            [
                'name' => 'Renault Premium Route (2006 г.)',
                'game_id' => 1,
            ],
            [
                'name' => 'Scania R (2004 г.)',
                'game_id' => 1,
            ],
            [
                'name' => 'Scania Streamline (2013 г.)',
                'game_id' => 1,
            ],
            [
                'name' => 'Scania (2017 г.)',
                'game_id' => 1,
            ],
            [
                'name' => 'Volvo FH16 (2012 г.)',
                'game_id' => 1,
            ],
            [
                'name' => 'Volvo FH (2009 г.)',
                'game_id' => 1,
            ],
            [
                'name' => 'Renault Truck (2013 г.)',
                'game_id' => 1,
            ],
            [
                'name' => 'Škoda Superb',
                'game_id' => 1,
            ],
            [
                'name' => 'Kenworth w900 (2008 г.)',
                'game_id' => 2,
            ],
            [
                'name' => 'Kenworth T680 (2014 г.)',
                'game_id' => 2,
            ],
            [
                'name' => 'Peterbilt 579 (2015 г.)',
                'game_id' => 2,
            ],
            [
                'name' => 'Peterbilt 389 (2007 г.)',
                'game_id' => 2,
            ],
            [
                'name' => 'Volvo VNL (2008 г.)',
                'game_id' => 2,
            ],
            [
                'name' => 'International Lonestar (2008 г.)',
                'game_id' => 2,
            ],
            [
                'name' => 'Škoda Superb',
                'game_id' => 2,
            ],
        ];

        foreach ($trucks as $truck) {
            if (!Trucks::where('name', $truck['name'])->where('game_id', $truck['game_id'])->count()) {
                Trucks::create([
                    'name' => $truck['name'],
                    'game_id' => $truck['game_id'],
                ]);

                echo "\nДобавлен грузовик: " . $truck['name'] . "\n";
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    private function dlcSeed(): bool
    {
        $response_ets = $this->getGuzzleHttp('https://api.truckyapp.com/v2/map/dlcs?game=ets2');

        if ($response_ets['response']) {
            foreach ($response_ets['response'] as $ets) {
                if (!DLC::where(['name' => $ets['name'], 'game_id' => 1])->count()) {
                    $d = new DLC();
                    $d->name = $ets['name'];
                    $d->game_id = 1;
                    $d->order = $ets['order'];
                    $d->save();
                    echo "\nДобавлен новый ДЛС для ETS2: " . $ets['name'];
                }
            }

            echo 'DLC для игры Euro Truck Simulator 2 были добавлены.';
        } else {
            echo $response_ets;
            return false;
        }

        $response_ats = $this->getGuzzleHttp('https://api.truckyapp.com/v2/map/dlcs?game=ats');

        if ($response_ats['response']) {
            foreach ($response_ats['response'] as $ats) {
                if (!DLC::where(['name' => $ats['name'], 'game_id' => 2])->count()) {
                    $d = new DLC();
                    $d->name = $ats['name'];
                    $d->game_id = 2;
                    $d->order = $ats['order'];
                    $d->save();
                    echo "\nДобавлен новый ДЛС для ATS: " . $ats['name'];
                }
            }

            echo 'DLC для игры American Truck Simulator были добавлены.';
        } else {
            echo $response_ats;
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    private function countryAndCitySeed(): bool
    {
        $response = $this->getGuzzleHttp('https://api.truckyapp.com/v2/map/cities/all');

        foreach ($response['response'] as $d) {
            if (!Countries::where(['name' => $d['country']])->count()) {
                $countries = new Countries();
                $countries->game_id = Game::where('shortName', strtoupper($d['game']))->first()->id;
                $countries->name = $d['country'];
                $countries->dlc_id = DLC::where('name', $d['dlc'])->first()->id;
                $countries->save();

                echo "\n Добавлена новая страна: " . $d['country'];
            }

            if (!Cities::where(['name' => $d['realName']])->count()) {
                $city = new Cities();
                $city->country_id = Countries::where(['name' => $d['country']])->first()->id;
                $city->name = $d['realName'];

                if (DLC::where('name', $d['dlc'])->first()) {
                    $city->dlc_id = DLC::where('name', $d['dlc'])->first()->id;
                } else {
                    $city->dlc_id = 0;
                }

                $city->save();

                echo "\n Добавлен новый город: " . $d['realName'];
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    private function truckersMPSeed(): bool
    {
        $servers = $this->getGuzzleHttp('https://api.truckersmp.com/v2/servers');

        if ($servers['error'] === 'false') {
            echo "\nРезультат получен от TruckersMP API...\n";

            foreach ($servers['response'] as $server) {
                if (!Servers::where('actual_id', $server['id'])->count()) {
                    $s = new Servers();
                    $s->game_id = Game::where('shortName', $server['game'])->first()->id;
                    $s->online = $server['online'];
                    $s->actual_id = $server['id'];
                    $s->name = $server['name'];
                    $s->shortName = $server['shortname'];
                    $s->save();

                    echo "\nСервер был добавлен: " . $server['name'];
                }
            }

            return true;
        }

        echo "\nВозникла ошибка при отправки запроса на TruckersMP API...\n";
        return false;
    }

    /**
     * @param string $url
     * @return array
     */
    private function getGuzzleHttp(string $url): array
    {
        $request = new GuzzleHttp\Client();
        echo "\nОтправляю запрос на: " . $url;
        $response = $request->get($url);

        return json_decode($response->getBody(), true);
    }
}
