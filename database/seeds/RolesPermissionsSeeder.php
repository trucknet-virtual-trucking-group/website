<?php

use Illuminate\Database\Seeder;
use HttpOz\Roles\Models\Role;

class RolesPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $roles = [
            ['Admin', 'admin', 'Администратор', 'administration'],
            ['Moderator', 'moderator', 'Модератор', 'administration'],
            ['Donator', 'donator', 'Донатор', null],
            ['Tester', 'tester', 'Тестировщик', null],
            ['User', 'user', 'Дальнобойщик', null],
            ['Banned', 'banned', 'Забаненный', 'restricted'],
        ];

        foreach ($roles as $role) {
            if (!Role::where(['name' => $role[0]])->count()) {
                Role::create([
                    'name' => $role[0],
                    'slug' => $role[1],
                    'description' => $role[2],
                    'group' => $role[3],
                ]);
            }
        }
    }
}
