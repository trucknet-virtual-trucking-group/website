<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConstants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('games', static function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->unique()->index();
            $table->string('shortName')->unique();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('servers', static function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('game_id');
            $table->boolean('online');
            $table->integer('actual_id');
            $table->string('name')->index();
            $table->string('shortName');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('countries', static function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('game_id');
            $table->string('name');
            $table->string('dlc_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('cities', static function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('country_id');
            $table->string('name');
            $table->string('dlc_id');
            $table->timestamps();
            $table->softDeletes();

            $table->index(['name']);
        });

        Schema::create('dlc', static function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->integer('game_id');
            $table->integer('order');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('games');
        Schema::dropIfExists('servers');
        Schema::dropIfExists('countries');
        Schema::dropIfExists('cities');
        Schema::dropIfExists('dlc');
    }
}
