<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('users', static function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->index();

            $table->string('firstName')->nullable();
            $table->string('lastName')->nullable();
            $table->date('dateOfBirth')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->integer('gender')->nullable();

            $table->string('login')->nullable();

            $table->string('nickname')->index()->nullable();
            $table->string('nickname_color')->nullable();

            $table->string('email')->unique()->nullable()->index();
            $table->timestamp('email_verified_at')->nullable();

            $table->string('password')->nullable();

            $table->bigInteger('steam_id')->nullable();
            $table->string('steam_username')->nullable();
            $table->string('steam_avatar')->nullable();

            $table->integer('avatar_id')->default(0);
            $table->integer('cover_id')->default(0);

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
}
