<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTruckersMpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('truckers_mp', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('mp_id');
            $table->string('name');
            $table->string('avatar');
            $table->string('smallAvatar');
            $table->string('groupName');
            $table->integer('groupID');
            $table->boolean('banned');
            $table->dateTime('bannedUntil')->nullable();
            $table->boolean('displayBans');
            $table->string('bans')->nullable();
            $table->boolean('inGameAdmin');
            $table->string('vtc')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('truckers_mp');
    }
}
