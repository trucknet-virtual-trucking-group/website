<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_settings', static function (Blueprint $table) {
            $table->increments('id')->unsigned()->index();

            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->bigInteger('vk_id')->nullable();
            $table->string('youtube')->nullable();
            $table->string('instagram')->nullable();
            $table->string('twitch')->nullable();

            $table->integer('ats_preferred_server')->unsigned()->nullable()->index();
            $table->foreign('ats_preferred_server')->references('id')->on('servers')->onDelete('cascade');

            $table->integer('ets_preferred_server')->unsigned()->nullable()->index();
            $table->foreign('ets_preferred_server')->references('id')->on('servers')->onDelete('cascade');

            $table->integer('ats_preferred_truck')->unsigned()->nullable()->index();
            $table->foreign('ats_preferred_truck')->references('id')->on('trucks')->onDelete('cascade');

            $table->integer('ets_preferred_truck')->unsigned()->nullable()->index();
            $table->foreign('ets_preferred_truck')->references('id')->on('trucks')->onDelete('cascade');

            $table->bigInteger('truckers_mp_id')->nullable();
            $table->string('truckers_mp_username')->nullable();
            $table->string('truckers_mp_avatar')->nullable();
            $table->boolean('truckers_mp_banned')->default(false);

            $table->boolean('rules_accepted')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_settings');
    }
}
