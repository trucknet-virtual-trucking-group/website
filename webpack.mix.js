const mix = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy( 'resources/images', 'public/images');

mix.copy('resources/images/favicon.ico', 'public/images');
mix.js('resources/js/bootstrap.js', 'public/js/vendor.min.js');

mix.scripts([
    'resources/js/helpers.js',
    'resources/js/notifications.js',
    'resources/js/common.js',
], 'public/js/common.js');

mix.sass('resources/sass/app.scss', 'public/css/common.css');

mix.version();
